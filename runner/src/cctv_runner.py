#!/usr/bin/env python3

import os.path
import re
import subprocess
import sys
import typing as tp
from collections import namedtuple
from datetime import datetime


_CCTV_GENERAL_CONFIG_KEYS = {
    'local_dir',
    'records_dir',
    'cctv_config',
    'resolution',
    'fps',
    'hls_segment_sec',
    'hls_list_size',
}

CctvGeneralConfig = namedtuple('CctvGeneralConfig', _CCTV_GENERAL_CONFIG_KEYS)


class CctvConfig:
    _GENERAL_REGEX = re.compile(
        r'(\S+)'  # key
        r'\s+'    # whitespace
        r'(\S+)'  # value
    )

    _CCTV_REGEX = re.compile(
        r'(\S+)'  # name
        r'\s+'    # whitespace
        r'(\S+)'  # url
        r'\s+'    # whitespace
        r'(\S+)'  # enabled
    )

    @classmethod
    def _load_general_config(cls, config_path: str) -> CctvGeneralConfig:
        config_dict = dict()
        with open(config_path, 'r') as config_file:
            for line in config_file:
                key, value = cls._GENERAL_REGEX.match(line).groups()
                if key in _CCTV_GENERAL_CONFIG_KEYS:
                    config_dict[key] = value

        return CctvGeneralConfig(**config_dict)

    @classmethod
    def _load_cctv_config(cls, config_path: str) -> tp.Dict[str, dict]:
        cameras = dict()
        with open(config_path, 'r') as config_file:
            for line in config_file:
                name, url, enabled = cls._CCTV_REGEX.match(line).groups()
                if name not in cameras:
                    cameras[name] = {
                        'url': url,
                        'enabled': enabled == 'enabled',
                    }
                else:
                    raise KeyError(f'Duplicate name in cctv config: {name}')

        return cameras

    def __init__(self, config_path: str):
        self._config = self._load_general_config(config_path)
        cctv_config_path = os.path.join(self._config.local_dir, self._config.cctv_config)
        self._cameras = self._load_cctv_config(cctv_config_path)

    @property
    def fps(self) -> int:
        return int(self._config.fps)

    @property
    def resolution(self) -> str:
        return self._config.resolution

    @property
    def hls_segment_sec(self) -> int:
        return int(self._config.hls_segment_sec)

    @property
    def hls_list_size(self) -> int:
        return int(self._config.hls_list_size)

    def url_for(self, name: str) -> str:
        return self._cameras[name]['url']

    def segment_template_for(self, name: str) -> str:
        basename = f'cam_{name}/%Y-%m-%d/T%H-%M-%S.ts'
        return os.path.join(self._config.records_dir, basename)

    def m3u8_path_for(self, name: str, now: datetime) -> str:
        basename = f'cam_{name}/{now:%Y-%m-%d/}/T{now:%H-%M-%S}'
        prefix = os.path.join(self._config.records_dir, basename)

        # If cam_...-23-59.m3u8 exists,
        # try cam_...-23-59_0.m3u8 next,
        # then cam_...-23-59_1.m3u8, etc.
        if os.path.exists(f'{prefix}.m3u8'):
            i = 0
            while os.path.exists(f'{prefix}_{i}.m3u8'):
                i += 1
            return f'{prefix}_{i}.m3u8'
        else:
            return f'{prefix}.m3u8'


class FfmpegInstance:
    def __init__(
        self,
        cam_name: str,
        global_options: tp.List[str],
        url: str,
        timeout_us: int,
        resolution: str,
        fps: int,
        gop: int,
        hls_seg_duration: str,
        hls_list_size: int,
        hls_name_template: str,
        destination_m3u8: str,
    ):
        overlay_text = ' '.join([
            f'cam\\:{cam_name}',
            'time\\:%{localtime\\:%F_%T%z}',
            'pts\\:%{pts\\:hms}',
        ])
        video_filter = f"drawtext=text='{overlay_text}':box=1:boxcolor=white@0.2" 
        self._cmdline = ['/usr/bin/ffmpeg']
        self._cmdline.extend(global_options)
        self._cmdline.extend([
            # input
            '-stimeout', str(timeout_us),
            '-i', url,
            # basic output parameters
            '-s', resolution,
            '-r', str(fps),
            # overlay text: camera name, local time, ptsa
            '-vf', video_filter,
            # x264 encoder parameters
            '-map', '0:v',
            '-flags', '+cgop',
            '-g', str(gop),
            '-c:v', 'libx264',
            '-preset', 'veryfast',
            '-tune', 'stillimage',
            '-b:v', '250k',
            '-x264-params', 'vbv-maxrate=400:vbv-bufsize=2000:force-cfr=1:nal-hrd=cbr',
            # mp3 encoder parameters
            # '-map', '0:a',
            '-c:a', 'mp3',
            # hls format parameters
            '-f', 'hls',
            '-hls_flags', 'temp_file-delete_segments',
            '-use_localtime', '1',
            '-strftime', '1',
            '-strftime_mkdir', '1',
            '-hls_segment_filename', hls_name_template,
            '-hls_time', hls_seg_duration,
            '-hls_list_size', str(hls_list_size),
            # destination
            destination_m3u8,
        ])

    def run(self):
        subprocess.check_call(self._cmdline)


class CctvInstance:
    _GLOBAL_OPTIONS = [
        '-hide_banner',
        '-loglevel',
        'warning',
    ]

    _TIMEOUT_US = 10_000_000

    def __init__(self, name: str, config: CctvConfig):
        self._name = name
        self._config = config

    def run(self):
        ffmpeg_instance = FfmpegInstance(
            self._name,
            self._GLOBAL_OPTIONS,
            self._config.url_for(self._name),
            self._TIMEOUT_US,
            self._config.resolution,
            self._config.fps,
            gop=10 * self._config.fps,
            hls_seg_duration=str(self._config.hls_segment_sec),
            hls_list_size=self._config.hls_list_size,
            hls_name_template=self._config.segment_template_for(self._name),
            destination_m3u8=self._config.m3u8_path_for(self._name, datetime.now()),
        )
        ffmpeg_instance.run()


def main():
    if len(sys.argv) < 2:
        print(f'Usage: {sys.argv[0]} config_path camera_name', file=sys.stderr)
        sys.exit(1)

    config_path, camera_name = sys.argv[1:3]
    config = CctvConfig(config_path)
    
    cctv_instance = CctvInstance(camera_name, config)
    cctv_instance.run()


if __name__ == '__main__':
    main()
