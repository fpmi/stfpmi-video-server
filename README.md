# stfpmi-cctv

## runner

1. Читает конфиг
2. Форкается в ffmpeg, записывающий RTSP с камеры в локальную ФС
3. Падает при таймауте соединения (но бывают спецэффекты, TODO)

См. [cctv_runner.py](./runner/src/cctv_runner.py)

## video-server

1. Устанавливается на один сервер с runner-ами
2. Индексирует видео, записанное runner-ами
3. По запросу генерирует HLS-плейлисты для просмотра записей в браузере
4. Авторизует запросы к сегментам (интегрируется в nginx через auth_request)

См. [video-server readme](./video-server/README.md)

## frontend

Минимальный UI: SPA с плеером и формой "введи токен, выбери поток, выбери дату"

См. [frontend](./frontend/)

## ansible

Плейбуки

Для каждого видео-сервера:
1. Раскладывает конфиги runner и video-server
2. Раскладывает systemd-юниты runner и video-server
3. Запускает нужное кол-во systemd-юнитов runner (под обслуживаемые конкретным сервером камеры)
4. TODO конфиг nginx для видеосервера с auth_request
5. TODO конфиг nginx для "балансировщика", маршрутизирующего запросы /svs/api и /svs/segments по stream_id в URL

См. [ansible](./ansible/)
