import { useMemo } from "react"
import { z } from "zod";

const configSchema = z.object({
  apiBaseUrl: z.string(),
  cameras: z.array(z.string()).min(1),
  helpText: z.string(),
});

export const useConfig = () => {
  return useMemo(() => {
    return configSchema.parse((window as any).__CONFIG__);
  }, []);
}
