import { useState } from 'react';
import { useCookie } from 'react-use';
import { useConfig } from '../config';
import { PlayerInput, PlayerInputForm } from './PlayerInputs';
import { HlsPlayer } from './HlsPlayer';
import dayjs from 'dayjs';

export type PlayerPageProps = React.HtmlHTMLAttributes<HTMLDivElement>;

export const PlayerPage = (props: PlayerPageProps) => {
  const { apiBaseUrl, cameras } = useConfig();
  const [playerInputs, setPlayerInputs] = useState<PlayerInput>({ date: dayjs() });
  const loginUrl = `${apiBaseUrl}/login/`;
  const [token, _] = useCookie('auth_token');
  if (!token) {
    window.open(loginUrl, '_self');
  }
  return <div className='PlayerPage' {...props}>
    <PlayerInputForm
      cameras={cameras}
      value={playerInputs}
      onInput={diff => {
        setPlayerInputs({...playerInputs, ...diff});
      }}
    />
    { (playerInputs.camera && playerInputs.date)
      ? <HlsPlayer
          controls={true}
          autoPlay={true}
          src={`${apiBaseUrl}/playlists/cam_${playerInputs.camera}/by_date/${playerInputs.date.format('YYYY-MM-DD')}`}
        />
      : <p>Выбери камеру и дату</p>}
    <p><a href={loginUrl}>Обновить токен</a> (попробуй, если видео не грузится)</p>
  </div>;
}
