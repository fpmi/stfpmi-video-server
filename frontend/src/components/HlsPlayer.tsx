import Hls from 'hls.js';
import { useEffect, useMemo, useRef } from 'react';

export type HlsPlayerProps = React.VideoHTMLAttributes<HTMLVideoElement> & {
  src: string;
};

export const HlsPlayer = ({ src, ...props }: HlsPlayerProps) => {
  const hls = useMemo(() => {
    const hls = new Hls({
      xhrSetup: (xhr, url) => {
        xhr.withCredentials = true;
      }
    });

    hls.on(Hls.Events.MEDIA_ATTACHED, () => {
      hls.loadSource(src);
    });

    hls.on(Hls.Events.MANIFEST_PARSED, (_event, data) => {
      console.log(`manifest loaded, quality levels: ${JSON.stringify(data.levels)}`);
    });

    hls.on(Hls.Events.ERROR, (_event, data) => {
      console.error(`hls.js error, fatal: ${data.fatal}, type: ${data.type}, details: ${JSON.stringify(data.details)}`);
      switch (data.details) {
        case Hls.ErrorDetails.FRAG_LOAD_ERROR:
          // ....
          break;
        default:
          break;
      }
    });

    return hls;
  }, [src]);

  const videoRef = useRef<HTMLVideoElement>(null);
  useEffect(() => {
    if (videoRef.current) {
      hls.attachMedia(videoRef.current);
    }
  }, [hls, videoRef]);

  if (!Hls.isSupported()) {
    throw new Error('HLS not supported');
  }
  return <video ref={videoRef} {...props}/>
};
