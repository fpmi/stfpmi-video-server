import { Grid, MenuItem, Select, TextField } from '@mui/material';
import { DatePicker } from '@mui/x-date-pickers';
import { Dayjs } from 'dayjs';

export type PlayerInput = {
  camera?: string | null;
  date?: Dayjs | null;
};

export type PlayerInputsProps = {
  cameras: string[];
  value: PlayerInput;
  onInput: (diff: Partial<PlayerInput>) => void;
}

export const PlayerInputForm = ({ cameras, value, onInput }: PlayerInputsProps) => {
  return <Grid>
    <Select
      value={value.camera}
      onChange={event => onInput({ camera: event.target.value })}
    >
      {cameras.map(cam =>
        <MenuItem key={cam} value={cam}>
          {cam}
        </MenuItem>)}
    </Select>
    <DatePicker
      value={value.date}
      onChange={date => onInput({ date })}
    />
  </Grid>
}
