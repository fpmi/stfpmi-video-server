import React from 'react';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import 'dayjs/locale/ru';
import { PlayerPage } from './components/PlayerPage';
import './App.css';
import { useConfig } from './config';

function App() {
  
  const { helpText } = useConfig();
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="ru">
      <div className='App'>
        <p>{helpText}</p>
        <PlayerPage />
      </div>
    </LocalizationProvider>
  );
}

export default App;
