import os.path
from datetime import date, timedelta

import plyvel
import pytest

from stfpmi_video_server.video.cache import (
    DictFsCache,
    FsCache,
    LeveldbFsCache,
    NopFsCache,
)
from stfpmi_video_server.video.index import from_directory
from .video_mocks import (  # noqa
    mock_ffprobe_json,
    mock_ffprobe_unclean_exit,
    mock_os_scandir_isdir,
)


class TestFromDirectory:
    _DATA_DIR = os.path.join(os.path.dirname(__file__), 'data')
    PREFIX = _DATA_DIR

    CACHE_IMPLEMENTATIONS = [
        NopFsCache(),
        DictFsCache(),
        LeveldbFsCache(plyvel.DB(os.path.join(_DATA_DIR, 'leveldb/test_video_index'), create_if_missing=True)),
    ]

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_three_ranges(self, cache: FsCache, mock_os_scandir_isdir, mock_ffprobe_json):  # noqa
        directory = os.path.join(self.PREFIX, "three_ranges")
        with cache.session() as cache_session:
            ranges = from_directory(directory, cache_session)

        assert len(ranges) == 3

        assert len(ranges[0].fragments) == 2
        assert ranges[0].duration == timedelta(seconds=10)

        assert len(ranges[1].fragments) == 1
        assert ranges[1].duration == timedelta(seconds=5)

        assert len(ranges[2].fragments) == 2
        assert ranges[2].duration == timedelta(seconds=10)

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_empty_directory(self, cache: FsCache, mock_os_scandir_isdir):  # noqa
        directory = os.path.join(self.PREFIX, "empty_directory")
        with cache.session() as cache_session:
            ranges = from_directory(directory, cache_session)

        assert len(ranges) == 0

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_handles_exceptions(self, cache: FsCache, mock_os_scandir_isdir, mock_ffprobe_unclean_exit):  # noqa
        # start_ts = "1970-01-01T00:13:37+00:00"
        # path = f"{self.PREFIX}/ffprobe_unclean_exit--{start_ts}.ts"
        #
        directory = os.path.join(self.PREFIX, "handles_exceptions")

        with cache.session() as cache_session:
            ranges = from_directory(directory, cache_session)
        assert len(ranges) == 0

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_reentrant_with_cache(self, cache: FsCache):
        data_dir = os.path.join(os.path.dirname(__file__), 'data')
        directory = os.path.join(data_dir, "index/stream_0")

        def iteration():
            with cache.session() as cache_session:
                ranges = from_directory(directory, cache_session)

            assert len(ranges) == 9

            assert len(ranges[0].fragments) == 1
            assert int(ranges[0].duration.total_seconds()) == 30

            assert len(ranges[1].fragments) == 1
            assert int(ranges[1].duration.total_seconds()) == 29

            assert len(ranges[2].fragments) == 1
            assert int(ranges[2].duration.total_seconds()) == 30

        for _ in range(5):
            iteration()

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_date_filter_no_unneeded_scans(self, cache: FsCache, mock_os_scandir_isdir, mock_ffprobe_json, recwarn):  # noqa
        directory = os.path.join(self.PREFIX, "date_filter_no_unneeded_scans")
        # 02..03
        with cache.session() as cache_session:
            ranges = from_directory(
                directory,
                cache_session,
                start_date=date(1970, 1, 2),
                end_date=date(1970, 1, 4),
            )

        assert len(ranges) == 1 + 1

        assert len(ranges[0].fragments) == 2
        assert ranges[0].duration == timedelta(seconds=10)

        assert len(ranges[1].fragments) == 1
        assert ranges[1].duration == timedelta(seconds=5)

        # Unneeded directory scan -> missing mock MpegTS data -> MpegTS scan failure -> warning
        # No warnings -> no unneeded directory scans
        assert len(recwarn) == 0

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_date_filter_different_ranges(self, cache: FsCache, mock_os_scandir_isdir, mock_ffprobe_json):  # noqa
        directory = os.path.join(self.PREFIX, "date_filter_different_ranges")

        # 01..02
        with cache.session() as cache_session:
            ranges_1_2 = from_directory(
                directory,
                cache_session,
                start_date=date(1970, 1, 1),
                end_date=date(1970, 1, 3),
            )

        assert len(ranges_1_2) == 3 + 1

        assert len(ranges_1_2[0].fragments) == 2
        assert ranges_1_2[0].duration == timedelta(seconds=10)
        assert len(ranges_1_2[1].fragments) == 1
        assert ranges_1_2[1].duration == timedelta(seconds=5)
        assert len(ranges_1_2[2].fragments) == 2
        assert ranges_1_2[2].duration == timedelta(seconds=10)

        assert len(ranges_1_2[0].fragments) == 2
        assert ranges_1_2[3].duration == timedelta(seconds=10)

        # 01..04
        with cache.session() as cache_session:
            ranges_1_4 = from_directory(
                directory,
                cache_session,
                start_date=date(1970, 1, 1),
                end_date=date(1970, 1, 5),
            )

        assert len(ranges_1_4) == 3 + 1 + 1 + 3

        assert len(ranges_1_4[0].fragments) == 2
        assert ranges_1_4[0].duration == timedelta(seconds=10)
        assert len(ranges_1_4[1].fragments) == 1
        assert ranges_1_4[1].duration == timedelta(seconds=5)
        assert len(ranges_1_4[2].fragments) == 2
        assert ranges_1_4[2].duration == timedelta(seconds=10)

        assert len(ranges_1_4[3].fragments) == 2
        assert ranges_1_4[3].duration == timedelta(seconds=10)

        assert len(ranges_1_4[4].fragments) == 1
        assert ranges_1_4[4].duration == timedelta(seconds=5)

        assert len(ranges_1_4[5].fragments) == 2
        assert ranges_1_4[5].duration == timedelta(seconds=10)
        assert len(ranges_1_4[6].fragments) == 1
        assert ranges_1_4[6].duration == timedelta(seconds=5)
        assert len(ranges_1_4[7].fragments) == 2
        assert ranges_1_4[7].duration == timedelta(seconds=10)

        # 02..03
        with cache.session() as cache_session:
            ranges_2_3 = from_directory(
                directory,
                cache_session,
                start_date=date(1970, 1, 2),
                end_date=date(1970, 1, 4),
            )

        assert len(ranges_2_3) == 1 + 1

        assert len(ranges_2_3[0].fragments) == 2
        assert ranges_2_3[0].duration == timedelta(seconds=10)

        assert len(ranges_2_3[1].fragments) == 1
        assert ranges_2_3[1].duration == timedelta(seconds=5)
