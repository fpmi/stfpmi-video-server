import os.path
import typing as tp
from datetime import datetime, timedelta

import plyvel
import pytest

from stfpmi_video_server.video.cache import (
    DictFsCache,
    FsCache,
    LeveldbFsCache,
    NopFsCache,
)
from stfpmi_video_server.video.hls import (
    MediaPlaylist,
    M3UPlaylistBuilder,
)
from stfpmi_video_server.video.index import from_directory
from stfpmi_video_server.video.mpegts import (
    MpegTSFile,
    MpegTSRange,
)
from .video_mocks import (  # noqa
    mock_ffprobe_json,
    mock_os_scandir_isdir,
)


class TestM3UPlaylistBuilder():
    def test_empty(self):
        b = M3UPlaylistBuilder()
        assert '#EXTM3U' == b.render()

    def test_x_tag_simple(self):
        b = M3UPlaylistBuilder()
        b.x_tag('independent-segments')

        expected = [
            '#EXTM3U',
            '#EXT-X-INDEPENDENT-SEGMENTS',
        ]
        assert expected == b.render().split('\n')

    def test_x_tag_with_value(self):
        b = M3UPlaylistBuilder()
        b.x_tag('version', str(7))

        expected = [
            '#EXTM3U',
            '#EXT-X-VERSION:7',
        ]
        assert expected == b.render().split('\n')

    def test_x_tag_with_named_attributes(self):
        b = M3UPlaylistBuilder()
        b.x_tag('start', time_offset=19.1, extra='extravalue')

        expected = [
            '#EXTM3U',
            '#EXT-X-START:TIME-OFFSET=19.1,EXTRA=extravalue',
        ]
        assert expected == b.render().split('\n')

    def test_three_x_tags(self):
        b = M3UPlaylistBuilder()
        b.x_tag('version', str(7))
        b.x_tag('independent-segments')
        b.x_tag('start', time_offset=19.1, extra='extravalue')

        expected = [
            '#EXTM3U',
            '#EXT-X-VERSION:7',
            '#EXT-X-INDEPENDENT-SEGMENTS',
            '#EXT-X-START:TIME-OFFSET=19.1,EXTRA=extravalue',
        ]
        assert expected == b.render().split('\n')


class TestMediaPlaylist():
    _DATA_DIR = os.path.join(os.path.dirname(__file__), 'data')
    PREFIX = _DATA_DIR

    CACHE_IMPLEMENTATIONS = [
        NopFsCache(),
        DictFsCache(),
        LeveldbFsCache(plyvel.DB(os.path.join(_DATA_DIR, 'leveldb/test_video_hls'), create_if_missing=True)),
    ]

    def make_playlist(self, ranges: tp.List[MpegTSRange], **kwargs: tp.Any):
        defaults = {
            'prefix': self.PREFIX,
            'segment_uri_prefix': '',
            'gap_uri': '/404',
            'live': True,
        }
        defaults.update(kwargs)
        return MediaPlaylist(ranges, **defaults)

    def test_empty_live(self):
        playlist = self.make_playlist([])

        expected = [
            '#EXTM3U',
            '#EXT-X-VERSION:2',
            '#EXT-X-INDEPENDENT-SEGMENTS',
        ]
        assert expected == playlist.render().split('\n')

    def test_empty_not_live(self):
        playlist = self.make_playlist([], live=False)

        expected = [
            '#EXTM3U',
            '#EXT-X-VERSION:2',
            '#EXT-X-INDEPENDENT-SEGMENTS',
            '#EXT-X-ENDLIST',
        ]
        assert expected == playlist.render().split('\n')

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_three_ranges(self, cache: FsCache, mock_ffprobe_json, mock_os_scandir_isdir):  # noqa
        directory = os.path.join(self.PREFIX, "three_ranges")
        with cache.session() as cache_session:
            ranges = from_directory(directory, cache_session)

        playlist = self.make_playlist(ranges)

        expected = [
            # Common header
            '#EXTM3U',
            '#EXT-X-VERSION:2',
            '#EXT-X-INDEPENDENT-SEGMENTS',
            '#EXT-X-TARGETDURATION:5',
            '#EXT-X-MEDIA-SEQUENCE:0',
            '#EXT-X-PLAYLIST-TYPE:event',
            # Range 0
            # |--Sequence 0
            '#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:13:37+03:00',
            '#EXTINF:5.0',
            'three_ranges/1970-01-01/T00-13-37.ts',
            # +--Sequence 1
            '#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:13:42+03:00',
            '#EXTINF:5.0',
            'three_ranges/1970-01-01/T00-13-42.ts',
            # Gap 0-1
            '#EXT-X-GAP',
            '#EXTINF:55.0',
            '/404',
            # Range 1
            '#EXT-X-DISCONTINUITY',
            # +--Sequence 0
            '#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:14:42+03:00',
            '#EXTINF:5.0',
            'three_ranges/1970-01-01/T00-14-42.ts',
            # Gap 1-2
            '#EXT-X-GAP',
            '#EXTINF:55.0',
            '/404',
            # Range 2
            '#EXT-X-DISCONTINUITY',
            # |--Sequence 0
            '#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:15:42+03:00',
            '#EXTINF:5.0',
            'three_ranges/1970-01-01/T00-15-42.ts',
            # +--Sequence 1
            '#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:15:47+03:00',
            '#EXTINF:5.0',
            'three_ranges/1970-01-01/T00-15-47.ts',
        ]
        assert expected == playlist.render().split('\n')

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_append_current_range_ok(self, cache: FsCache, mock_ffprobe_json):  # noqa
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:13:39+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = f"{self.PREFIX}/sequence_second/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)

        playlist = self.make_playlist([MpegTSRange([first])])
        playlist.append_current_range(second)

        expected = [
            # Common header
            '#EXTM3U',
            '#EXT-X-VERSION:2',
            '#EXT-X-INDEPENDENT-SEGMENTS',
            '#EXT-X-TARGETDURATION:5',
            '#EXT-X-MEDIA-SEQUENCE:0',
            '#EXT-X-PLAYLIST-TYPE:event',
            # Range 0
            # |--Sequence 0
            '#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:13:37+03:00',
            '#EXTINF:5.0',
            'sequence_first/1970-01-01/T00-13-37.ts',
            # |--Sequence 1
            '#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:13:39+03:00',
            '#EXTINF:5.0',
            'sequence_second/1970-01-01/T00-13-39.ts',
        ]
        assert expected == playlist.render().split('\n')

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_start_new_range(self, cache: FsCache, mock_ffprobe_json):  # noqa
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:20:37+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = (
            f"{self.PREFIX}/sequence_first_plus_frame/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"
        )

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)

        playlist = self.make_playlist([MpegTSRange([first])])
        playlist.start_new_range(second)
        expected = [
            # Common header
            '#EXTM3U',
            '#EXT-X-VERSION:2',
            '#EXT-X-INDEPENDENT-SEGMENTS',
            '#EXT-X-TARGETDURATION:5',
            '#EXT-X-MEDIA-SEQUENCE:0',
            '#EXT-X-PLAYLIST-TYPE:event',
            # Range 0
            # |--Sequence 0
            '#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:13:37+03:00',
            '#EXTINF:5.0',
            'sequence_first/1970-01-01/T00-13-37.ts',
            # Gap 0-1
            '#EXT-X-GAP',
            '#EXTINF:415.0',
            '/404',
            # Range 1
            '#EXT-X-DISCONTINUITY',
            # |--Sequence 0
            '#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:20:37+03:00',
            '#EXTINF:5.0',
            'sequence_first_plus_frame/1970-01-01/T00-20-37.ts',
        ]
        assert expected == playlist.render().split('\n')

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_truncate_no_change(self, cache: FsCache, mock_ffprobe_json, mock_os_scandir_isdir):  # noqa
        directory = os.path.join(self.PREFIX, "three_ranges")
        with cache.session() as cache_session:
            ranges = from_directory(directory, cache_session)

        playlist = self.make_playlist(ranges.copy())
        assert len(playlist.ranges) == 3
        assert playlist.ranges[0] == ranges[0]
        assert playlist.initial_mseq == 0

        playlist.truncate_until_duration_under(timedelta(seconds=25))
        assert len(playlist.ranges) == 3
        assert playlist.ranges[0] == ranges[0]
        assert playlist.initial_mseq == 0

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_truncate_one_first_whole(self, cache: FsCache, mock_ffprobe_json, mock_os_scandir_isdir):  # noqa
        directory = os.path.join(self.PREFIX, "three_ranges")
        with cache.session() as cache_session:
            ranges = from_directory(directory, cache_session)

        playlist = self.make_playlist(ranges.copy())
        assert len(playlist.ranges) == 3
        assert playlist.ranges[0] == ranges[0]
        assert playlist.initial_mseq == 0

        playlist.truncate_until_duration_under(timedelta(seconds=19.99))
        assert len(playlist.ranges) == 2
        assert playlist.ranges[0] == ranges[1]
        assert playlist.initial_mseq == 3

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_truncate_one_first_partial(self, cache: FsCache, mock_ffprobe_json, mock_os_scandir_isdir):  # noqa
        directory = os.path.join(self.PREFIX, "three_ranges")
        with cache.session() as cache_session:
            ranges = from_directory(directory, cache_session)

        playlist = self.make_playlist(ranges.copy())
        assert len(playlist.ranges) == 3
        assert len(playlist.ranges[0].fragments) == 2
        assert playlist.initial_mseq == 0

        playlist.truncate_until_duration_under(timedelta(seconds=24.99))
        assert len(playlist.ranges) == 3
        assert len(playlist.ranges[0].fragments) == 1
        assert playlist.initial_mseq == 1

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_truncate_two_first_whole(self, cache: FsCache, mock_ffprobe_json, mock_os_scandir_isdir):  # noqa
        directory = os.path.join(self.PREFIX, "three_ranges")
        with cache.session() as cache_session:
            ranges = from_directory(directory, cache_session)

        playlist = self.make_playlist(ranges.copy())
        assert len(playlist.ranges) == 3
        assert playlist.ranges[0] == ranges[0]
        assert playlist.initial_mseq == 0

        playlist.truncate_until_duration_under(timedelta(seconds=14.99))
        assert len(playlist.ranges) == 1
        assert playlist.ranges[0] == ranges[2]
        assert playlist.initial_mseq == 5

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_truncate_all_but_last_partial(self, cache: FsCache, mock_ffprobe_json, mock_os_scandir_isdir):  # noqa
        directory = os.path.join(self.PREFIX, "three_ranges")
        with cache.session() as cache_session:
            ranges = from_directory(directory, cache_session)

        playlist = self.make_playlist(ranges.copy())
        assert len(playlist.ranges) == 3
        assert len(playlist.ranges[0].fragments) == 2
        assert playlist.initial_mseq == 0

        playlist.truncate_until_duration_under(timedelta(seconds=9.99))
        assert len(playlist.ranges) == 1
        assert len(playlist.ranges[0].fragments) == 1
        assert playlist.initial_mseq == 6

    @pytest.mark.parametrize('cache', CACHE_IMPLEMENTATIONS)
    def test_truncate_all(self, cache: FsCache, mock_ffprobe_json, mock_os_scandir_isdir):  # noqa
        directory = os.path.join(self.PREFIX, "three_ranges")
        with cache.session() as cache_session:
            ranges = from_directory(directory, cache_session)

        playlist = self.make_playlist(ranges.copy())
        assert len(playlist.ranges) == 3
        assert len(playlist.ranges[0].fragments) == 2
        assert playlist.initial_mseq == 0

        playlist.truncate_until_duration_under(timedelta(seconds=4.99))
        assert len(playlist.ranges) == 0
        assert playlist.initial_mseq == 7
