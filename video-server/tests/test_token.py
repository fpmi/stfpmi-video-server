import datetime
import typing as tp
import fastapi
import pytest
from stfpmi_video_server.config import ConfigHolder
import stfpmi_video_server.token
from stfpmi_video_server.token import check_auth_token, AuthJWTPayload, ViewPermission

SECRET_KEY = ConfigHolder.get().jwt_secret_key.get_secret_value()


def make_token(
    permissions: tp.Optional[tp.List[ViewPermission]] = None,
    not_before: tp.Optional[datetime.datetime] = None,
    expiry: tp.Optional[datetime.datetime] = None,
    algorithm: str = "HS256",
    secret_key: str = SECRET_KEY,
) -> str:
    model = AuthJWTPayload.model_construct(
        permissions=permissions,
        not_before=not_before,
        expiry=expiry
    )
    return model.encode(algorithm, secret_key)


@pytest.fixture
def with_config(monkeypatch: tp.Any) -> None:
    config = ConfigHolder.get()
    monkeypatch.setattr(
        stfpmi_video_server.token.ConfigHolder,
        "get",
        lambda: config
    )


@pytest.fixture
def now() -> datetime.datetime:
    return datetime.datetime.now() \
        .astimezone(datetime.timezone.utc) \
        .replace(microsecond=0)


def test_valid_with_empty_permissions(with_config, now: datetime.datetime):  # type: ignore
    permissions = []
    not_before = now - datetime.timedelta(seconds=1)
    expiry = now + datetime.timedelta(seconds=10)
    token = make_token(permissions, not_before, expiry)

    parsed_jwt = check_auth_token(token)
    assert parsed_jwt.not_before == not_before
    assert parsed_jwt.expiry == expiry
    assert len(parsed_jwt.permissions) == 0


def test_valid_with_two_permissions(with_config, now: datetime.datetime):  # type: ignore
    permissions = [
        ViewPermission(
            stream_id="2ka-test-1",
            start_ts=now,
            end_ts=now + datetime.timedelta(hours=1)
        ),
        ViewPermission(
            stream_id="2ka-test-2",
            start_ts=now,
            end_ts=now + datetime.timedelta(hours=2)
        )
    ]
    not_before = now - datetime.timedelta(seconds=1)
    expiry = now + datetime.timedelta(hours=2, seconds=1)
    token = make_token(permissions, not_before, expiry)

    parsed_jwt = check_auth_token(token)
    assert parsed_jwt.not_before == not_before
    assert parsed_jwt.expiry == expiry
    assert parsed_jwt.permissions == permissions


def test_missing_not_before(with_config, now: datetime.datetime):  # type: ignore
    token = make_token(
        permissions=[],
        not_before=None,
        expiry=now + datetime.timedelta(seconds=10)
    )

    with pytest.raises(fastapi.HTTPException) as exc_info:
        _ = check_auth_token(token)

    assert exc_info.type == fastapi.HTTPException
    e = exc_info.value
    assert e.status_code == 403
    assert e.detail == "Invalid JWT token: Token is missing the \"nbf\" claim"


def test_missing_expiry(with_config, now: datetime.datetime):  # type: ignore
    token = make_token(
        permissions=[],
        not_before=now - datetime.timedelta(seconds=1),
        expiry=None
    )

    with pytest.raises(fastapi.HTTPException) as exc_info:
        _ = check_auth_token(token)

    assert exc_info.type == fastapi.HTTPException
    e = exc_info.value
    assert e.status_code == 403
    assert e.detail == "Invalid JWT token: Token is missing the \"exp\" claim"


def test_missing_permissions_key(with_config, now: datetime.datetime):  # type: ignore
    token = make_token(
        permissions=None,
        not_before=now - datetime.timedelta(seconds=1),
        expiry=now + datetime.timedelta(seconds=10)
    )

    with pytest.raises(fastapi.HTTPException) as exc_info:
        _ = check_auth_token(token)

    assert exc_info.type == fastapi.HTTPException
    e = exc_info.value
    assert e.status_code == 403
    assert e.detail.startswith("Invalid payload:")
    assert "\npermissions\n" in e.detail


def test_token_from_future(with_config, now: datetime.datetime):  # type: ignore
    token = make_token(
        permissions=[],
        not_before=now + datetime.timedelta(seconds=1),
        expiry=now + datetime.timedelta(seconds=10)
    )

    with pytest.raises(fastapi.HTTPException) as exc_info:
        _ = check_auth_token(token)

    assert exc_info.type == fastapi.HTTPException
    e = exc_info.value
    assert e.status_code == 403
    assert e.detail.startswith("Invalid JWT token: The token is not yet valid (nbf)")


def test_expired_token(with_config, now: datetime.datetime):  # type: ignore
    token = make_token(
        permissions=[],
        not_before=now - datetime.timedelta(seconds=10),
        expiry=now - datetime.timedelta(seconds=1)
    )

    with pytest.raises(fastapi.HTTPException) as exc_info:
        _ = check_auth_token(token)

    assert exc_info.type == fastapi.HTTPException
    e = exc_info.value
    assert e.status_code == 403
    assert e.detail.startswith("Invalid JWT token: Signature has expired")


def test_invalid_key(with_config, now: datetime.datetime):  # type: ignore
    token = make_token(
        permissions=[],
        not_before=now - datetime.timedelta(seconds=1),
        expiry=now + datetime.timedelta(seconds=10),
        secret_key="invalid-secret-key"
    )

    with pytest.raises(fastapi.HTTPException) as exc_info:
        _ = check_auth_token(token)

    assert exc_info.type == fastapi.HTTPException
    e = exc_info.value
    assert e.status_code == 403
    assert e.detail.startswith("Invalid JWT token: Signature verification failed")
