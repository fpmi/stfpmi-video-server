from datetime import datetime, timedelta

from fastapi.testclient import TestClient
from fastapi import status

from stfpmi_video_server.config import Config
from .conftest import jwt_for


def test_get_playlist_content_all(app, config: Config, now: datetime):
    stream_id = "stream_0"
    perm_start_ts = datetime.fromisoformat('1970-01-01T00:00:00+03:00')
    perm_end_ts = now + timedelta(seconds=30)
    encoded_token = jwt_for(config, stream_id, perm_start_ts, perm_end_ts, now)

    client = TestClient(app)
    client.cookies.set("auth_token", encoded_token)
    rsp = client.get(
        f"/playlists/{stream_id}/content.m3u8",
        params={
            "start_ts": int(perm_start_ts.timestamp()),
            "end_ts": int(perm_end_ts.timestamp()),
        },
    )
    assert rsp.status_code == 200
    segment_uri_prefix = config.segment_uri_prefix
    assert rsp.content.decode('utf-8').split("\n") == [
        "#EXTM3U",
        "#EXT-X-VERSION:2",
        "#EXT-X-INDEPENDENT-SEGMENTS",
        "#EXT-X-TARGETDURATION:30",
        "#EXT-X-MEDIA-SEQUENCE:0",
        "#EXT-X-PLAYLIST-TYPE:event",

        "#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:13:37+03:00",
        "#EXTINF:30.023222",
        f"{segment_uri_prefix}/stream_0/1970-01-01/T00-13-37.ts",

        "#EXT-X-GAP",
        "#EXTINF:4.976778",
        "/404",

        "#EXT-X-DISCONTINUITY",
        "#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:14:12+03:00",
        "#EXTINF:29.837633",
        f"{segment_uri_prefix}/stream_0/1970-01-01/T00-14-12.ts",

        "#EXT-X-GAP",
        "#EXTINF:3535.162367",
        "/404",

        "#EXT-X-DISCONTINUITY",
        "#EXT-X-PROGRAM-DATE-TIME:1970-01-01T01:13:37+03:00",
        "#EXTINF:30.023222",
        f"{segment_uri_prefix}/stream_0/1970-01-01/T01-13-37.ts",

        "#EXT-X-GAP",
        "#EXTINF:82769.976778",
        "/404",

        "#EXT-X-DISCONTINUITY",
        "#EXT-X-PROGRAM-DATE-TIME:1970-01-02T00:13:37+03:00",
        "#EXTINF:30.023222",
        f"{segment_uri_prefix}/stream_0/1970-01-02/T00-13-37.ts",

        "#EXT-X-GAP",
        "#EXTINF:4.976778",
        "/404",

        "#EXT-X-DISCONTINUITY",
        "#EXT-X-PROGRAM-DATE-TIME:1970-01-02T00:14:12+03:00",
        "#EXTINF:29.837633",
        f"{segment_uri_prefix}/stream_0/1970-01-02/T00-14-12.ts",

        "#EXT-X-GAP",
        "#EXTINF:3535.162367",
        "/404",

        "#EXT-X-DISCONTINUITY",
        "#EXT-X-PROGRAM-DATE-TIME:1970-01-02T01:13:37+03:00",
        "#EXTINF:30.023222",
        f"{segment_uri_prefix}/stream_0/1970-01-02/T01-13-37.ts",

        "#EXT-X-GAP",
        "#EXTINF:82769.976778",
        "/404",

        "#EXT-X-DISCONTINUITY",
        "#EXT-X-PROGRAM-DATE-TIME:1970-01-03T00:13:37+03:00",
        "#EXTINF:30.023222",
        f"{segment_uri_prefix}/stream_0/1970-01-03/T00-13-37.ts",

        "#EXT-X-GAP",
        "#EXTINF:4.976778",
        "/404",

        "#EXT-X-DISCONTINUITY",
        "#EXT-X-PROGRAM-DATE-TIME:1970-01-03T00:14:12+03:00",
        "#EXTINF:29.837633",
        f"{segment_uri_prefix}/stream_0/1970-01-03/T00-14-12.ts",

        "#EXT-X-GAP",
        "#EXTINF:3535.162367",
        "/404",

        "#EXT-X-DISCONTINUITY",
        "#EXT-X-PROGRAM-DATE-TIME:1970-01-03T01:13:37+03:00",
        "#EXTINF:30.023222",
        f"{segment_uri_prefix}/stream_0/1970-01-03/T01-13-37.ts",
    ]


def test_get_playlist_content_filter_by_date(app, config: Config, now: datetime):
    stream_id = "stream_0"
    perm_start_ts = datetime.fromisoformat('1970-01-01T00:00:00+03:00')
    perm_end_ts = now + timedelta(seconds=30)
    encoded_token = jwt_for(config, stream_id, perm_start_ts, perm_end_ts, now)

    client = TestClient(app)
    client.cookies.set("auth_token", encoded_token)
    rsp = client.get(
        f"/playlists/{stream_id}/content.m3u8?start_ts=-10800&end_ts=75720",
        follow_redirects=False,
    )
    assert rsp.status_code == 200
    segment_uri_prefix = config.segment_uri_prefix
    assert rsp.content.decode('utf-8').split("\n") == [
        "#EXTM3U",
        "#EXT-X-VERSION:2",
        "#EXT-X-INDEPENDENT-SEGMENTS",
        "#EXT-X-TARGETDURATION:30",
        "#EXT-X-MEDIA-SEQUENCE:0",
        "#EXT-X-PLAYLIST-TYPE:event",

        "#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:13:37+03:00",
        "#EXTINF:30.023222",
        f"{segment_uri_prefix}/stream_0/1970-01-01/T00-13-37.ts",

        "#EXT-X-GAP",
        "#EXTINF:4.976778",
        "/404",

        "#EXT-X-DISCONTINUITY",
        "#EXT-X-PROGRAM-DATE-TIME:1970-01-01T00:14:12+03:00",
        "#EXTINF:29.837633",
        f"{segment_uri_prefix}/stream_0/1970-01-01/T00-14-12.ts",

        "#EXT-X-GAP",
        "#EXTINF:3535.162367",
        "/404",

        "#EXT-X-DISCONTINUITY",
        "#EXT-X-PROGRAM-DATE-TIME:1970-01-01T01:13:37+03:00",
        "#EXTINF:30.023222",
        f"{segment_uri_prefix}/stream_0/1970-01-01/T01-13-37.ts",
    ]


def test_get_playlist_representations(app, config: Config, now: datetime):
    stream_id = "stream_0"
    perm_start_ts = datetime.fromisoformat('1970-01-01T00:00:00+03:00')
    perm_end_ts = now + timedelta(seconds=30)
    encoded_token = jwt_for(config, stream_id, perm_start_ts, perm_end_ts, now)

    client = TestClient(app)
    client.cookies.set("auth_token", encoded_token)
    rsp = client.get(
        f"/playlists/{stream_id}?start_ts=-10800&end_ts=75720",
        follow_redirects=False,
    )
    assert rsp.status_code == 200
    assert rsp.content.decode('utf-8').split("\n") == [
        "#EXTM3U",
        "#EXT-X-VERSION:3",
        "#EXT-X-STREAM-INF:BANDWIDTH=126551,CODECS=\"avc1.4d0014\",RESOLUTION=352x240",
        "stream_0/content.m3u8?start_ts=-10800&end_ts=75720",
    ]


def test_get_playlist_by_date_redirect(app, config: Config, now: datetime):
    stream_id = "stream_0"
    perm_start_ts = datetime.fromisoformat('1970-01-01T00:00:00+03:00')
    perm_end_ts = now + timedelta(seconds=30)
    encoded_token = jwt_for(config, stream_id, perm_start_ts, perm_end_ts, now)

    client = TestClient(app)
    client.cookies.set("auth_token", encoded_token)
    for_date = perm_start_ts.date()
    rsp = client.get(
        f"/playlists/{stream_id}/by_date/{for_date.isoformat()}",
        follow_redirects=False,
    )
    assert rsp.status_code == status.HTTP_302_FOUND
    # Negative start timestamp iiwii, never happens irl
    assert rsp.headers['location'] == f'{config.base_url}/playlists/stream_0?start_ts=-10800&end_ts=75720'
