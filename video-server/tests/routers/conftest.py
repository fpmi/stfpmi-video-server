from datetime import datetime, timedelta, timezone

import pytest
from prometheus_client import REGISTRY

from stfpmi_video_server.config import Config, ConfigHolder
from stfpmi_video_server.main import app_factory
from stfpmi_video_server.token import AuthJWTPayload, ViewPermission


@pytest.fixture(autouse=True)
def reset_metrics():
    collectors = list(REGISTRY._collector_to_names.keys())
    for collector in collectors:
        REGISTRY.unregister(collector)


@pytest.fixture(scope="package")
def config() -> Config:
    return ConfigHolder.get()


@pytest.fixture(scope="package")
def app():
    app = app_factory()
    yield app

    del app


@pytest.fixture
def now() -> datetime:
    return datetime.now() \
        .astimezone(timezone.utc) \
        .replace(microsecond=0)


def jwt_for(config: Config, stream_id: str, since: datetime, until: datetime, now) -> str:
    permissions = [
        ViewPermission(
            stream_id=stream_id,
            start_ts=since,
            end_ts=until
        )
    ]
    token = AuthJWTPayload.model_construct(
        not_before=now - timedelta(seconds=10),
        expiry=now + timedelta(seconds=30),
        permissions=permissions
    )
    return token.encode(
        config.jwt_algorithm,
        config.jwt_secret_key.get_secret_value()
    )
