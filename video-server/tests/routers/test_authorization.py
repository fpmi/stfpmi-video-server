from datetime import datetime, timedelta

from fastapi.testclient import TestClient

from stfpmi_video_server.config import Config
from .conftest import jwt_for


def test_auth_valid(app, config: Config, now: datetime):
    perm_start_ts = datetime.fromisoformat('2000-01-01T00:17:02+03:00')
    perm_end_ts = now + timedelta(seconds=30)
    encoded_token = jwt_for(config, 'stream_0', perm_start_ts, perm_end_ts, now)

    client = TestClient(app)
    client.cookies.set("auth_token", encoded_token)
    rsp = client.get(
        "/auth",
        headers={
            "X-Original-URI": f'{config.segment_uri_prefix}/stream_0/2000-01-01/T00-17-02.ts',
        },
    )
    assert rsp.status_code == 202
    assert rsp.content.decode('utf-8') == '{}'


def test_auth_no_valid(app, config: Config, now: datetime):
    perm_start_ts = datetime.fromisoformat('2000-01-01T00:17:02+03:00')
    perm_end_ts = perm_start_ts + timedelta(seconds=30)
    encoded_token = jwt_for(config, 'stream-a', perm_start_ts, perm_end_ts, now)

    client = TestClient(app)
    client.cookies.set("auth_token", encoded_token)
    rsp = client.get(
        "/auth",
        headers={
            "X-Original-URI": f'{config.segment_uri_prefix}/stream-another/2023-01-15/T18-55-02.ts',
        },
    )
    expected = '{"detail":"No permission for stream \'stream-another\'"}'
    assert rsp.status_code == 403
    assert rsp.content.decode('utf-8') == expected


def test_auth_regex_no_match(app, config: Config, now: datetime):
    perm_start_ts = datetime.fromisoformat('2000-01-01T00:17:02+03:00')
    perm_end_ts = perm_start_ts + timedelta(seconds=30)
    encoded_token = jwt_for(config, 'stream-a', perm_start_ts, perm_end_ts, now)

    client = TestClient(app)
    client.cookies.set("auth_token", encoded_token)
    rsp = client.get(
        "/auth",
        headers={
            "X-Original-URI": '/admin',
        },
    )
    assert rsp.status_code == 403
    assert rsp.content.decode('utf-8') == '{"detail":"URL doesn\'t match regex"}'


def test_auth_no_x_original_uri(app, config: Config, now: datetime):
    perm_start_ts = datetime.fromisoformat('2000-01-01T00:17:02+03:00')
    perm_end_ts = perm_start_ts + timedelta(seconds=30)
    encoded_token = jwt_for(config, 'stream-a', perm_start_ts, perm_end_ts, now)

    client = TestClient(app)
    client.cookies.set("auth_token", encoded_token)
    rsp = client.get(
        "/auth",
    )
    assert rsp.status_code == 403
    assert rsp.content.decode('utf-8') == '{"detail":"Missing header X-Original-URI"}'


def test_auth_no_token(app, config: Config, now: datetime):
    client = TestClient(app)
    rsp = client.get(
        "/auth",
        headers={
            "X-Original-URI": f'{config.segment_uri_prefix}/stream-a/2023-01-15/T18-55-02.ts',
        },
    )
    assert rsp.status_code == 401
    assert rsp.content.decode('utf-8') == '{"detail":"Missing authorization token"}'
