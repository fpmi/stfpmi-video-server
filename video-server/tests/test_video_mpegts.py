import subprocess
from datetime import datetime, timedelta

import pytest

from stfpmi_video_server.video.mpegts import MpegTSFile, MpegTSRange
from .video_mocks import (  # noqa
    mock_ffprobe_json,
    mock_ffprobe_unclean_exit,
)


class TestParsePath:
    def test_simple(self):
        path = "stream-id/2022-04-19/T14-00-00.ts"
        stream_id, start_wall_time = MpegTSFile._parse_path(path)
        assert stream_id == "stream-id"
        assert start_wall_time == datetime(
            year=2022, month=4, day=19,
            hour=14, minute=0, second=0,
        ).astimezone()

    def test_many_double_hyphens(self):
        path = "2ka--room-name--meta/2022-04-19/T14-00-00.ts"
        stream_id, start_wall_time = MpegTSFile._parse_path(path)
        assert stream_id == "2ka--room-name--meta"
        assert start_wall_time == datetime(
            year=2022, month=4, day=19,
            hour=14, minute=0, second=0,
            tzinfo=None,
        ).astimezone()

    def test_no_t(self):
        path = "stream-id/2022-04-19/14-00-00.ts"
        with pytest.raises(ValueError) as exc_info:
            _, _ = MpegTSFile._parse_path(path)

        expected_msg = "time data '2022-04-19/14-00-00' does not match format '%Y-%m-%d/T%H-%M-%S'"
        assert exc_info.value.args == (expected_msg,)

    def test_invalid_extension(self):
        path = "stream-id/2022-04-19/T14-00-00.avi"
        with pytest.raises(ValueError) as exc_info:
            _, _ = MpegTSFile._parse_path(path)

        expected_msg = "Invalid extension: .avi"
        assert exc_info.value.args == (expected_msg,)

    def test_no_extension(self):
        path = "stream-id/2022-04-19/T14-00-00"
        with pytest.raises(ValueError) as exc_info:
            _, _ = MpegTSFile._parse_path(path)

        expected_msg = "Invalid extension: "
        assert exc_info.value.args == (expected_msg,)


class TestProbe:
    PREFIX = "/nonexistent/mocked"

    def test_first_valid(self, mock_ffprobe_json):  # noqa
        start_ts_s = "1970-01-01T00:13:37+03:00"
        start_ts = datetime.fromisoformat(start_ts_s)
        path = f"{self.PREFIX}/first_valid/{start_ts:%Y-%m-%d}/T{start_ts:%H-%M-%S}.ts"
        mpegts_file = MpegTSFile.from_path(path)

        assert mpegts_file.path == path
        assert mpegts_file.size_bytes == 852493

        assert mpegts_file.start_wall_ts == start_ts
        assert mpegts_file.duration == timedelta(seconds=7.475556)
        assert mpegts_file.start_pts == 4167967
        assert mpegts_file.end_pts == 6159878

    def test_missing_program(self, mock_ffprobe_json):  # noqa
        start_ts_s = "1970-01-01T00:13:37+03:00"
        start_ts = datetime.fromisoformat(start_ts_s)
        path = f"{self.PREFIX}/missing_program/{start_ts:%Y-%m-%d}/T{start_ts:%H-%M-%S}.ts"

        with pytest.raises(ValueError) as exc_info:
            _ = MpegTSFile.from_path(path)

        expected_msg = "programs\n  List should have at least 1 item after validation, not 0"
        assert expected_msg in str(exc_info)

    def test_ffprobe_unclean_exit(self, mock_ffprobe_unclean_exit):  # noqa
        start_ts_s = "1970-01-01T00:13:37+03:00"
        start_ts = datetime.fromisoformat(start_ts_s)
        path = f"{self.PREFIX}/ffprobe_unclean_exit/{start_ts:%Y-%m-%d}/T{start_ts:%H-%M-%S}.ts"

        with pytest.raises(subprocess.SubprocessError):
            _ = MpegTSFile.from_path(path)


class TestIsContinuation:
    """
    JSONs from MpegTS files generated with
    $ ffmpeg \
        -r 15 \
        -i /dev/video2 \
        -c:v h264_nvenc \
            -flags +cgop -g 15 \
        -f hls \
            -hls_time 5 \
            -hls_flags -delete_segments \
        x.m3u8
    """

    PREFIX = "/nonexistent/mocked"

    def test_actually_is_continuation(self, mock_ffprobe_json):  # noqa
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:13:39+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = f"{self.PREFIX}/sequence_second/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)

        assert second.is_continuation_for(first)

    def test_hole_between_files(self, mock_ffprobe_json):  # noqa
        """
        sequence_first_plus_frame.json = sequence_first.json
            + 1 frame in start_pts & end_pts
        """
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:20:37+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = (
            f"{self.PREFIX}/sequence_first_plus_frame/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"
        )

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)

        assert not second.is_continuation_for(first)

    def test_second_before_first(self, mock_ffprobe_json):  # noqa
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:13:39+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = f"{self.PREFIX}/sequence_second/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)

        assert not first.is_continuation_for(second)


class TestRange:
    PREFIX = "/nonexistent/mocked"

    def test_single_range(self, mock_ffprobe_json):  # noqa
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:13:39+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = f"{self.PREFIX}/sequence_second/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)
        mpegts_range = MpegTSRange([first, second])

        assert mpegts_range.start_wall_ts == first_start_ts
        assert mpegts_range.duration == first.duration + second.duration
        assert mpegts_range.start_pts == first.start_pts
        assert mpegts_range.end_pts == second.end_pts
        assert len(mpegts_range.fragments) == 2

    def test_hole_between_files(self, mock_ffprobe_json):  # noqa
        """
        sequence_first_plus_frame.json = sequence_first.json
            + 1 frame in start_pts & end_pts
        """
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:20:37+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = (
            f"{self.PREFIX}/sequence_first_plus_frame/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"
        )

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)

        with pytest.raises(ValueError) as exc_info:
            _ = MpegTSRange([first, second])

        expected_msg = f"Not a MpegTS range: broken at {second_path}"
        assert exc_info.value.args == (expected_msg,)

    def test_second_before_first(self, mock_ffprobe_json):  # noqa
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:13:39+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = f"{self.PREFIX}/sequence_second/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)

        with pytest.raises(ValueError) as exc_info:
            _ = MpegTSRange([second, first])

        expected_msg = f"Not a MpegTS range: broken at {first_path}"
        assert exc_info.value.args == (expected_msg,)

    def test_empty_range(self):
        with pytest.raises(ValueError) as exc_info:
            _ = MpegTSRange(list())

        expected_msg = "Empty fragments list"
        assert exc_info.value.args == (expected_msg,)

    def test_append_valid(self, mock_ffprobe_json):  # noqa
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:13:39+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = f"{self.PREFIX}/sequence_second/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)
        mpegts_range = MpegTSRange([first])

        mpegts_range.append(second)
        assert len(mpegts_range.fragments) == 2
        assert mpegts_range.start_wall_ts == first_start_ts
        assert mpegts_range.duration == first.duration + second.duration
        assert mpegts_range.start_pts == first.start_pts
        assert mpegts_range.end_pts == second.end_pts

    def test_append_not_continuation(self, mock_ffprobe_json):  # noqa
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:20:37+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = (
            f"{self.PREFIX}/sequence_first_plus_frame/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"
        )

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)
        mpegts_range = MpegTSRange([first])

        with pytest.raises(ValueError) as exc_info:
            mpegts_range.append(second)

        expected_msg = "Not a continuation"
        assert exc_info.value.args == (expected_msg,)

    def test_truncate_no_change(self, mock_ffprobe_json):  # noqa
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:13:39+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = f"{self.PREFIX}/sequence_second/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)
        mpegts_range = MpegTSRange([first, second])

        mpegts_range.truncate_until_duration_under(timedelta(seconds=11))
        assert len(mpegts_range.fragments) == 2

    def test_truncate_one_first(self, mock_ffprobe_json):  # noqa
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:13:39+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = f"{self.PREFIX}/sequence_second/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)
        mpegts_range = MpegTSRange([first, second])

        mpegts_range.truncate_until_duration_under(timedelta(seconds=5.1))
        assert len(mpegts_range.fragments) == 1
        assert mpegts_range.fragments[0] == second

    def test_truncate_all(self, mock_ffprobe_json):  # noqa
        first_start_ts_s = "1970-01-01T00:13:37+03:00"
        first_start_ts = datetime.fromisoformat(first_start_ts_s)
        first_path = f"{self.PREFIX}/sequence_first/{first_start_ts:%Y-%m-%d}/T{first_start_ts:%H-%M-%S}.ts"

        second_start_ts_s = "1970-01-01T00:13:39+03:00"
        second_start_ts = datetime.fromisoformat(second_start_ts_s)
        second_path = f"{self.PREFIX}/sequence_second/{second_start_ts:%Y-%m-%d}/T{second_start_ts:%H-%M-%S}.ts"

        first = MpegTSFile.from_path(first_path)
        second = MpegTSFile.from_path(second_path)
        mpegts_range = MpegTSRange([first, second])

        mpegts_range.truncate_until_duration_under(timedelta(seconds=4.99))
        assert len(mpegts_range.fragments) == 0
