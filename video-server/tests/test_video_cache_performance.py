import logging
import os.path
import timeit

import plyvel
import pytest

from stfpmi_video_server.video.cache import (
    DictFsCache,
    FsCacheSession,
    LeveldbFsCache,
    NopFsCache,
)
from stfpmi_video_server.video.index import from_directory


class TestCachePerformance:
    _DATA_DIR = os.path.join(os.path.dirname(__file__), 'data')

    def iteration(self, directory: str, cache: FsCacheSession):
        ranges = from_directory(directory, cache)

        assert len(ranges) == 9

        assert len(ranges[0].fragments) == 1
        assert int(ranges[0].duration.total_seconds()) == 30

        assert len(ranges[1].fragments) == 1
        assert int(ranges[1].duration.total_seconds()) == 29

        assert len(ranges[2].fragments) == 1
        assert int(ranges[2].duration.total_seconds()) == 30

    @pytest.mark.slow
    def test_dict_cache_simple(self):
        logging.basicConfig(level=logging.CRITICAL)
        directory = os.path.join(self._DATA_DIR, "index/stream_0")
        iterations = 50

        dict_cache = DictFsCache()
        dict_sec = timeit.timeit(lambda: self.iteration(directory, dict_cache), number=iterations)

        nop_cache = NopFsCache()
        nop_sec = timeit.timeit(lambda: self.iteration(directory, nop_cache), number=iterations)

        speedup = nop_sec / dict_sec
        speedup_efficiency = speedup / iterations
        assert speedup_efficiency >= 0.8
        print(f'DictFsCache: expected speedup (# iterations) {iterations}, actual speedup {speedup}')

    @pytest.mark.slow
    def test_leveldb_cache_simple(self):
        logging.basicConfig(level=logging.CRITICAL)
        directory = os.path.join(self._DATA_DIR, "index/stream_0")
        iterations = 50

        database_name = os.path.join(self._DATA_DIR, "leveldb/test_video_cache_performance")
        try:
            plyvel.destroy_db(database_name)
        except Exception:
            pass

        db = plyvel.DB(database_name, create_if_missing=True)
        leveldb_cache = LeveldbFsCache(db.prefixed_db(b'perftest-'))

        def iteration():
            with leveldb_cache.session() as cache_session:
                self.iteration(directory, cache_session)

        leveldb_sec = timeit.timeit(iteration, number=iterations)

        nop_cache = NopFsCache()
        nop_sec = timeit.timeit(lambda: self.iteration(directory, nop_cache), number=iterations)

        speedup = nop_sec / leveldb_sec
        speedup_efficiency = speedup / iterations
        assert speedup_efficiency >= 0.6
        print(f'LeveldbFsCache: expected speedup (# iterations) {iterations}, actual speedup {speedup}')
