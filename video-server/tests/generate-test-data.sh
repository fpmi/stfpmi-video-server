#!/bin/sh -e
test_audio="-f lavfi -i sine=frequency=410:duration=30" 
encoders="-c:v libx264 -c:a aac"
script_dir="$(dirname "$0")"
index_dir="$script_dir/data/index"

out_dir="$index_dir/stream_0/1970-01-01"
mkdir -p $out_dir
ffmpeg -hide_banner -nostdin -loglevel error -y -f lavfi -i testsrc=duration=30:size=1280x720:rate=5 $test_audio $encoders "$out_dir/T00-13-37.ts"
ffmpeg -hide_banner -nostdin -loglevel error -y -f lavfi -ss 35 -i testsrc=duration=60:size=1280x720:rate=5 $test_audio $encoders "$out_dir/T00-14-12.ts"
ffmpeg -hide_banner -nostdin -loglevel error -y -f lavfi -i testsrc=duration=30:size=1280x720:rate=5 $test_audio $encoders "$out_dir/T01-13-37.ts"

out_dir="$index_dir/stream_0/1970-01-02"
mkdir -p $out_dir
ffmpeg -hide_banner -nostdin -loglevel error -y -f lavfi -i testsrc=duration=30:size=1280x720:rate=5 $test_audio $encoders "$out_dir/T00-13-37.ts"
ffmpeg -hide_banner -nostdin -loglevel error -y -f lavfi -ss 35 -i testsrc=duration=60:size=1280x720:rate=5 $test_audio $encoders "$out_dir/T00-14-12.ts"
ffmpeg -hide_banner -nostdin -loglevel error -y -f lavfi -i testsrc=duration=30:size=1280x720:rate=5 $test_audio $encoders "$out_dir/T01-13-37.ts"    

out_dir="$index_dir/stream_0/1970-01-03"
mkdir -p $out_dir
ffmpeg -hide_banner -nostdin -loglevel error -y -f lavfi -i testsrc=duration=30:size=1280x720:rate=5 $test_audio $encoders "$out_dir/T00-13-37.ts"
ffmpeg -hide_banner -nostdin -loglevel error -y -f lavfi -ss 35 -i testsrc=duration=60:size=1280x720:rate=5 $test_audio $encoders "$out_dir/T00-14-12.ts"
ffmpeg -hide_banner -nostdin -loglevel error -y -f lavfi -i testsrc=duration=30:size=1280x720:rate=5 $test_audio $encoders "$out_dir/T01-13-37.ts"  

out_dir="$index_dir/stream_0/1970-01-04"
mkdir -p $out_dir
