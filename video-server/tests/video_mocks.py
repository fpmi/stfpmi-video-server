from dataclasses import dataclass
import json
import os.path
import subprocess
import typing as tp

import pytest

import stfpmi_video_server.video.index.on_demand
import stfpmi_video_server.video.mpegts
from stfpmi_video_server.video.mpegts import MpegTSFile


@pytest.fixture
def mock_ffprobe_json(monkeypatch: tp.Any) -> None:
    data_dir = os.path.join(os.path.dirname(__file__), "data")

    def mocked_check_output(cmdline: tp.List[str], *_args: tp.Any, **_kwargs: tp.Any) -> bytes:
        filename = cmdline[-1]
        stream_id, start_ts = MpegTSFile._parse_path(filename)
        with open(os.path.join(data_dir, f"video_mpegts/{stream_id}/{start_ts:T%H-%M-%S}.json"), "r") as output:
            lines = output.readlines()
            return "\n".join(lines).encode()

    module = stfpmi_video_server.video.mpegts
    monkeypatch.setattr(module.subprocess, "check_output", mocked_check_output)


@pytest.fixture
def mock_ffprobe_unclean_exit(monkeypatch: tp.Any) -> None:
    def mocked_check_output(*args: tp.Any, **kwargs: tp.Any):
        raise subprocess.SubprocessError()

    module = stfpmi_video_server.video.mpegts
    monkeypatch.setattr(module.subprocess, "check_output", mocked_check_output)


@dataclass
class MockDirEntry:
    name: str

    def is_dir(self):
        return True

    def is_file(self):
        return True


@pytest.fixture
def mock_os_scandir_isdir(monkeypatch: tp.Any) -> None:
    data_dir = os.path.join(os.path.dirname(__file__), "data")

    def mocked_os_scandir(path: str) -> tp.List[tp.Any]:
        relpath = os.path.relpath(path, data_dir)
        with open(os.path.join(data_dir, f"video_index/{relpath}.json"), "r") as result_json:
            result = json.load(result_json)
            return [MockDirEntry(r["name"]) for r in result]

    module = stfpmi_video_server.video.index.on_demand
    monkeypatch.setattr(module.os, "scandir", mocked_os_scandir)
    monkeypatch.setattr(module.os.path, "isdir", lambda p: True)  # type: ignore
