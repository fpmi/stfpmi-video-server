from datetime import date
import logging
import os
import shutil
import time

import plyvel
import pytest
from prometheus_client import REGISTRY, Gauge, Histogram

from stfpmi_video_server.config import ConfigHolder
from stfpmi_video_server.video.cache import DictFsCache, LeveldbFsCache
from stfpmi_video_server.video.index import InotifyIndex


@pytest.fixture(autouse=True)
def reset_metrics():
    collectors = list(REGISTRY._collector_to_names.keys())
    for collector in collectors:
        REGISTRY.unregister(collector)


class TestInotify:
    _DATA_DIR = os.path.join(os.path.dirname(__file__), "data")
    PREFIX = _DATA_DIR

    CACHE_IMPLEMENTATIONS = ['dict', 'leveldb']

    @classmethod
    def make_cache(cls, key: str):
        if key == 'dict':
            return DictFsCache()
        elif key == 'leveldb':
            return LeveldbFsCache(
                plyvel.DB(
                    os.path.join(cls._DATA_DIR, "leveldb/test_video_inotify"),
                    create_if_missing=True,
                )
            )

    @pytest.fixture(autouse=True)
    def cleanup_index(self):
        database_name = os.path.join(self._DATA_DIR, "leveldb/test_video_inotify")
        index_dir = os.path.join(self._DATA_DIR, "index", "stream_0")
        keep_video_dirs = ["1970-01-01", "1970-01-02", "1970-01-03"]

        try:
            plyvel.destroy_db(database_name)
        except Exception:
            pass

        for dirname in os.scandir(index_dir):
            if dirname.name not in keep_video_dirs:
                shutil.rmtree(os.path.join(index_dir, dirname))

        yield
        # Post-test

        try:
            plyvel.destroy_db(database_name)
        except Exception:
            pass

        for dirname in os.scandir(index_dir):
            if dirname.name not in keep_video_dirs:
                shutil.rmtree(os.path.join(index_dir, dirname))

    @pytest.mark.parametrize('cache_type', CACHE_IMPLEMENTATIONS)
    def test_handler(self, cache_type: str):
        config = ConfigHolder.get()
        cache = self.make_cache(cache_type)
        index = InotifyIndex(
            config,
            "stream_0",
            cache=cache,
            last_lookup=Gauge("last_lookup", ""),
            lookup_duration=Histogram("lookup_duration", ""),
            last_update=Gauge("last_update", "", labelnames=["stream_id"]),
            logger=logging.getLogger("test"),
        )
        path = index.index_dir
        full_dirs = ["1970-01-01", "1970-01-02", "1970-01-03"]
        empty_dirs = ["1970-01-04"]

        def simple_check():
            with index.cache.session() as cache_session:
                ranges = index._ranges(
                    cache_session,
                    start_date=date(year=1970, month=1, day=1),
                    end_date=date(year=3000, month=1, day=1),
                )
                assert len(ranges) == len(full_dirs) * 3

        simple_check()

        shutil.copytree(path + "/1970-01-01", path + "/1970-01-04", dirs_exist_ok=True)
        time.sleep(2)  # actually necessary to kill asynchrony at that point!
        # if the tests dont pass, it may be bc of low sleep time; double it and check the results
        full_dirs.append(empty_dirs[0])
        empty_dirs.clear()
        simple_check()

        shutil.rmtree(path + "/1970-01-04")
        time.sleep(2)
        empty_dirs.append(full_dirs[3])
        full_dirs.pop()
        simple_check()

        for day in range(5, 20):
            dirname = f'1970-01-{day:02}'
            shutil.copytree(path + "/1970-01-01", path + "/" + dirname, dirs_exist_ok=True)
            full_dirs.append(dirname)
        time.sleep(15)  # actually necessary to kill asynchrony at that point!
        # if the tests dont pass, it may be bc of low sleep time; double it and check the results
        simple_check()

        index.close()

    @pytest.mark.slow
    @pytest.mark.parametrize('cache_type', CACHE_IMPLEMENTATIONS)
    def test_fast_restart_with_cache(self, cache_type: str):
        config = ConfigHolder.get()
        cache = self.make_cache(cache_type)

        path = os.path.join(self._DATA_DIR, "index", "stream_0")
        for day in range(4, 32):
            dirname = f'1970-01-{day:02}'
            shutil.copytree(path + "/1970-01-01", path + "/" + dirname, dirs_exist_ok=True)
        for day in range(1, 29):
            dirname = f'1970-02-{day:02}'
            shutil.copytree(path + "/1970-01-01", path + "/" + dirname, dirs_exist_ok=True)
        full_dirs = 59

        last_lookup = Gauge("last_lookup", "")
        lookup_duration = Histogram("lookup_duration", "")
        last_update = Gauge("last_update", "", labelnames=["stream_id"])

        before_init = time.time()
        # Initial sync in __init__
        index = InotifyIndex(
            config,
            "stream_0",
            cache=cache,
            last_lookup=last_lookup,
            lookup_duration=lookup_duration,
            last_update=last_update,
            logger=logging.getLogger("test"),
        )
        init_sec = time.time() - before_init
        assert init_sec < 20
        with index.cache.session() as cache_session:
            ranges = index._ranges(
                cache_session,
                start_date=date(year=1970, month=1, day=1),
                end_date=date(year=3000, month=1, day=1),
            )
            assert len(ranges) == full_dirs * 3

        before_restart = time.time()
        # Fast re-sync with cache in __init__
        index = InotifyIndex(
            config,
            "stream_0",
            cache=cache,
            last_lookup=last_lookup,
            lookup_duration=lookup_duration,
            last_update=last_update,
            logger=logging.getLogger("test"),
        )
        restart_sec = time.time() - before_restart
        assert restart_sec < 0.5
        with index.cache.session() as cache_session:
            ranges = index._ranges(
                cache_session,
                start_date=date(year=1970, month=1, day=1),
                end_date=date(year=3000, month=1, day=1),
            )
            assert len(ranges) == full_dirs * 3

        index.close()
