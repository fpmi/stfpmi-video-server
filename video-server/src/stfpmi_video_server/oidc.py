import jwt
import typing as tp
import requests
from dataclasses import dataclass
from pydantic import BaseModel


class OidcIssuerMetadata(BaseModel):
    authorization_endpoint: str
    id_token_signing_alg_values_supported: tp.List[str]
    issuer: str
    jwks_uri: str
    token_endpoint: str


@dataclass
class OidcIssuer:
    metadata: OidcIssuerMetadata
    jwk_client: jwt.PyJWKClient

    @classmethod
    def from_discovery(cls, discovery_url: str) -> 'OidcIssuer':
        metadata_rsp = requests.get(discovery_url)
        metadata_rsp.raise_for_status()
        metadata = OidcIssuerMetadata.parse_obj(metadata_rsp.json())
        jwks_client = jwt.PyJWKClient(metadata.jwks_uri)
        return cls(metadata, jwks_client)


class IdTokenPayload(BaseModel):
    sub: str
    svs_streams: tp.List[str] = []


class TokenResponse(BaseModel):
    id_token: str
    refresh_token: str
