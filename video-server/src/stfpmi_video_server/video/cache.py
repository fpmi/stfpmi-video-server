from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import datetime
import os
import typing as tp

import plyvel
from pydantic import BaseModel

from stfpmi_video_server.video.mpegts import FFMpegProbeResult, MpegTSFile


@dataclass
class CacheEntryDir:
    # Key: filename
    # Order of items must match file creation order
    files: tp.Dict[str, MpegTSFile]

    # True iff directory contents cannot change
    final: bool


class FsCacheSession(tp.ContextManager):
    @abstractmethod
    def get_directory(self, abs_path: str) -> tp.Optional[CacheEntryDir]:
        raise NotImplementedError()

    @abstractmethod
    def set_directory(self, abs_path: str, entry: CacheEntryDir) -> None:
        raise NotImplementedError()

    @abstractmethod
    def prune_directory(self, abs_path: str) -> None:
        raise NotImplementedError()

    def __enter__(self) -> 'FsCacheSession':
        return self

    def __exit__(self, *_exc) -> None:
        pass


class FsCache(ABC):
    @abstractmethod
    def session(self) -> FsCacheSession:
        raise NotImplementedError()

    def close(self) -> None:
        pass


class NopFsCache(FsCache, FsCacheSession):
    def get_directory(self, abs_path: str) -> tp.Optional[CacheEntryDir]:
        return None

    def set_directory(self, abs_path: str, entry: CacheEntryDir) -> None:
        pass

    def prune_directory(self, abs_path: str) -> None:
        pass

    def session(self) -> FsCacheSession:
        return self


class DictFsCache(FsCache, FsCacheSession):
    def __init__(self) -> None:
        self.entries: tp.Dict[str, CacheEntryDir] = {}

    def get_directory(self, abs_path: str) -> tp.Optional[CacheEntryDir]:
        return self.entries.get(abs_path)

    def set_directory(self, abs_path: str, entry: CacheEntryDir) -> None:
        self.entries[abs_path] = entry

    def prune_directory(self, abs_path: str) -> None:
        pass

    def session(self) -> FsCacheSession:
        return self


class LeveldbMpegTSFile(BaseModel):
    start_wall_ts: datetime
    probe_result: FFMpegProbeResult


class LeveldbCacheEntryDir(BaseModel):
    files: tp.Dict[str, LeveldbMpegTSFile]
    final: bool


class LeveldbFsCacheSession(FsCacheSession):
    _ENCODING = 'utf-8'

    def __init__(self, db: plyvel.DB) -> None:
        self._db = db
        self._wbatch = None
        self._snapshot = None
        self._diff = None
        self._prune_list = None

    def __enter__(self) -> 'LeveldbFsCacheSession':
        self._wbatch = self._db.write_batch(transaction=True)
        self._snapshot = self._db.snapshot()
        self._diff = DictFsCache()
        self._prune_list = []
        return self

    def __exit__(self, *_exc) -> None:
        assert self._snapshot is not None
        assert self._wbatch is not None
        assert self._prune_list is not None
        for to_delete in self._prune_list:
            self._wbatch.delete(to_delete.encode(self._ENCODING))

        self._snapshot.close()
        self._wbatch.write()

    def get_directory(self, abs_path: str) -> tp.Optional[CacheEntryDir]:
        assert self._diff is not None
        return self._diff.get_directory(abs_path) or self._persist_get_directory(abs_path)

    def set_directory(self, abs_path: str, entry: CacheEntryDir) -> None:
        assert self._diff is not None
        self._persist_set_directory(abs_path, entry)
        self._diff.set_directory(abs_path, entry)

    def prune_directory(self, abs_path: str) -> None:
        assert self._prune_list is not None
        self._prune_list.append(abs_path)

    def _persist_get_directory(self, abs_path: str) -> tp.Optional[CacheEntryDir]:
        assert self._snapshot is not None
        cache_key = abs_path
        ser_cache_value = self._snapshot.get(cache_key.encode(self._ENCODING))
        if ser_cache_value:
            parsed = LeveldbCacheEntryDir.model_validate_json(ser_cache_value.decode(self._ENCODING))
            return CacheEntryDir(
                files={
                    name: MpegTSFile(
                        os.path.join(abs_path, name),
                        mpfile.start_wall_ts,
                        mpfile.probe_result.model_dump(by_alias=True),
                    ) for name, mpfile in parsed.files.items()
                },
                final=parsed.final,
            )
        else:
            return None

    def _persist_set_directory(self, abs_path: str, entry: CacheEntryDir) -> None:
        assert self._wbatch is not None
        cache_key = abs_path
        cache_value = LeveldbCacheEntryDir(
            files={
                name: LeveldbMpegTSFile(
                    start_wall_ts=mpfile.start_wall_ts,
                    probe_result=mpfile.probe_result,
                )
                for name, mpfile in entry.files.items()
            },
            final=entry.final,
        )
        self._wbatch.put(
            cache_key.encode(self._ENCODING),
            cache_value.model_dump_json(by_alias=True).encode(self._ENCODING),
        )


class LeveldbFsCache(FsCache):
    def __init__(self, db: plyvel.DB) -> None:
        self.db = db

    def session(self) -> LeveldbFsCacheSession:
        return LeveldbFsCacheSession(self.db)

    def close(self) -> None:
        self.db.close()
