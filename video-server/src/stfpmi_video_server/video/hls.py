import os.path
import typing as tp
from datetime import timedelta

from stfpmi_video_server.video.mpegts import MpegTSFile, MpegTSRange


RangesList = tp.List[MpegTSRange]


class M3UPlaylistBuilder:
    def __init__(self):
        self.lines: tp.List[str] = []
        self.tag('extm3u')

    def comment(self, text: str) -> None:
        if '\n' in text:
            raise ValueError('Multi-line comment')

        self.lines.append('# Comment: ' + text)

    def tag(self, tag: str, value: tp.Optional[str] = None, **attrs: tp.Any) -> None:
        """
        Example:
            ``builder.tag('ext-x-start', time_offset=13.7)``
                -> 'EXT-X-START:TIME-OFFSET=13.7'

            ``builder.tag('ext-x-targetduration', 300)``
                -> 'EXT-X-TARGETDURATION:300'
        """
        if value is not None and len(attrs) > 0:
            raise ValueError('Both value and attrs specified')

        if len(attrs):
            def format_kv(kv: tp.Tuple[str, tp.Any]) -> str:
                k, v = kv
                k = k.replace('_', '-').upper()
                return f'{k}={v}'

            value = ':' + ','.join(map(format_kv, attrs.items()))
        elif value:
            value = ':' + value
        else:
            value = ''

        self.lines.append('#' + tag.upper() + value)

    def x_tag(self, tag: str, value: tp.Optional[str] = None, **attrs: tp.Any) -> None:
        self.tag('EXT-X-' + tag, value, **attrs)

    def uri(self, uri: str) -> None:
        # TODO validate uri syntax
        self.lines.append(uri)

    def render(self) -> str:
        return '\n'.join(self.lines)


class MediaPlaylist:
    def __init__(
            self,
            ranges: RangesList,
            prefix: str,
            segment_uri_prefix: str,
            gap_uri: str,
            live: bool,
            initial_mseq: int = 0,
    ):
        """
        Args:
            prefix: produce URIs relative to the specified prefix
            gap_uri: URI to specify for missing recordings, should return 404
        """
        self.ranges = ranges
        self.prefix = prefix
        self.segment_uri_prefix = segment_uri_prefix
        self.gap_uri = gap_uri
        self.live = live
        self.initial_mseq = initial_mseq

        if self.ranges:
            self.max_duration = max(map(lambda r: r.max_fragment_duration, ranges))
        else:
            self.max_duration = timedelta()

    def append_current_range(self, mfile: MpegTSFile) -> None:
        """
        Append `mfile` to the current range
        """
        self.ranges[-1].append(mfile)

    def start_new_range(self, mfile: MpegTSFile) -> None:
        """
        Append a discontinuity and start a new range with `mfile`
        """
        self.ranges.append(MpegTSRange([mfile]))

    def truncate_until_duration_under(self, limit: timedelta) -> None:
        """
        Truncate prefix so that leftover duration is under `limit`

        Correctly updates `self.initial_mseq`, see RFC section 4.4.3.2

        NOTE doesn't count gaps in duration
        """
        if limit.total_seconds() < 0:
            raise ValueError("Negative limit")
        elif not self.ranges:
            return

        updated_mseq = self.initial_mseq

        # Find earliest range to keep
        leftover_duration = timedelta()
        keep_idx = len(self.ranges) - 1
        while keep_idx >= 0:
            rng = self.ranges[keep_idx]
            if leftover_duration + rng.duration > limit:
                break
            else:
                leftover_duration += rng.duration
                keep_idx -= 1

        if keep_idx < 0:
            # May keep everything, nothing to do
            return

        # Increment initial_mseq according to dropped ranges
        # Each range contains N URIs plus one 404 URI for the gap
        dropped_ranges = self.ranges[:keep_idx]
        updated_mseq += sum(map(
            lambda r: len(r.fragments) + 1,
            dropped_ranges
        ))

        # Drop N first ranges whole
        self.ranges = self.ranges[keep_idx:]

        # Truncate the first kept range
        updated_mseq += self.ranges[0].truncate_until_duration_under(
            limit - leftover_duration
        )

        if len(self.ranges[0].fragments) == 0:
            # Truncated the whole first kept range
            # Then remove it completely
            self.ranges = self.ranges[1:]
            if len(self.ranges) > 0:
                # Also, if it wasn't the last, it contained one more 404 gap URI
                updated_mseq += 1

        self.initial_mseq = updated_mseq

    def render(self) -> str:
        builder = M3UPlaylistBuilder()
        builder.x_tag('version', str(2))
        builder.x_tag('independent-segments')

        if len(self.ranges) == 0:
            if not self.live:
                builder.x_tag('endlist')

            return builder.render()

        # builder.x_tag('start', time_offset=???)

        target_duration = round(self.max_duration.total_seconds())
        builder.x_tag('targetduration', str(target_duration))

        builder.x_tag('media-sequence', str(self.initial_mseq))

        builder.x_tag('playlist-type', 'event')

        # builder.x_tag('server-control', ???)

        mseq = self.render_range_to(self.ranges[0], builder, self.initial_mseq)
        for prev, curr in zip(self.ranges, self.ranges[1:]):
            gap = curr.start_wall_ts - prev.end_wall_ts
            mseq = self.render_gap_to(gap, builder, mseq)
            mseq = self.render_range_to(curr, builder, mseq)

        if not self.live:
            builder.x_tag('endlist')

        return builder.render()

    def render_range_to(
        self,
        rng: MpegTSRange,
        builder: M3UPlaylistBuilder,
        mseq: int,
    ) -> int:
        """
        Return value: updated mseq (Media Sequence Number)
        """
        for fragment in rng.fragments:
            builder.x_tag('program-date-time', fragment.start_wall_ts.isoformat())
            builder.tag('extinf', str(fragment.duration.total_seconds()))
            uri = self.segment_uri_prefix + os.path.relpath(fragment.path, self.prefix)
            builder.uri(uri)
            mseq += 1

        return mseq

    def render_gap_to(
        self,
        gap: timedelta,
        builder: M3UPlaylistBuilder,
        mseq: int
    ) -> int:
        """
        Return value: updated mseq (Media Sequence Number)
        """
        builder.x_tag('gap')
        builder.tag('extinf', str(gap.total_seconds()))
        builder.uri(self.gap_uri)
        builder.x_tag('discontinuity')
        return mseq + 1
