import json
import os
import subprocess
import typing as tp
from datetime import datetime, timedelta

from pydantic import BaseModel, Field


class FFMpegFormat(BaseModel):
    size_bytes: int = Field(alias='size')
    duration_sec: float = Field(alias='duration')


class FFMpegStream(BaseModel):
    start_pts: int
    duration_ts: int


class FFMpegProgram(BaseModel):
    streams: tp.List[FFMpegStream] = Field(min_length=1)


class FFMpegProbeResult(BaseModel):
    format: FFMpegFormat
    programs: tp.List[FFMpegProgram] = Field(min_length=1)


class MpegTSFile:
    EXTENSION = '.ts'

    @classmethod
    def from_path(cls, path: str) -> "MpegTSFile":
        _stream_id, start_wall_ts = cls._parse_path(path)

        ffprobe_cmdline = [
            "ffprobe",
            "-hide_banner",
            "-show_programs",
            "-show_format",
            "-print_format",
            "json",
            path
        ]
        ffprobe_stdout = subprocess.check_output(
            ffprobe_cmdline,
            stderr=subprocess.DEVNULL,
            preexec_fn=os.setpgrp,  # Don't receive parent-targeted signals
        ).decode()
        probe_result = json.loads(ffprobe_stdout)

        return cls(path, start_wall_ts, probe_result)

    @classmethod
    def _parse_path(cls, path: str) -> tp.Tuple[str, datetime]:
        """
        Return value: (stream_id, start_wall_ts)
        """
        dir_name, file_name = os.path.split(path)
        parent_dir_name, dir_name = os.path.split(dir_name)
        parent_dir_name = os.path.basename(parent_dir_name)
        file_name_without_ext, ext = os.path.splitext(file_name)
        if ext.lower() != cls.EXTENSION:
            raise ValueError(f"Invalid extension: {ext}")

        stream_id = parent_dir_name
        start_wall_ts = datetime.strptime(f"{dir_name}/{file_name_without_ext}", "%Y-%m-%d/T%H-%M-%S")
        start_wall_ts = start_wall_ts.astimezone()  # tz=None -> system local

        return stream_id, start_wall_ts

    def __init__(
        self,
        path: str,
        start_wall_ts: datetime,
        probe_result: tp.Dict[str, tp.Any],
    ):
        self.path = path
        self.start_wall_ts = start_wall_ts
        self.probe_result = FFMpegProbeResult.model_validate(probe_result)

    @property
    def size_bytes(self) -> int:
        return self.probe_result.format.size_bytes

    @property
    def end_wall_ts(self) -> datetime:
        return self.start_wall_ts + self.duration

    @property
    def duration(self) -> timedelta:
        sec = self.probe_result.format.duration_sec
        return timedelta(seconds=sec)

    @property
    def start_pts(self) -> int:
        stream = self.probe_result.programs[0].streams[0]
        return stream.start_pts

    @property
    def end_pts(self) -> int:
        stream = self.probe_result.programs[0].streams[0]
        return stream.start_pts + stream.duration_ts

    def is_continuation_for(self, other: "MpegTSFile") -> bool:
        return self.start_pts == other.end_pts


class MpegTSRange:
    def __init__(self, fragments: tp.List[MpegTSFile]):
        if not fragments:
            raise ValueError("Empty fragments list")

        for prev, curr in zip(fragments, fragments[1:]):
            if not curr.is_continuation_for(prev):
                raise ValueError(f"Not a MpegTS range: broken at {curr.path}")

        self.fragments_ = fragments

    def append(self, update: MpegTSFile) -> None:
        if not update.is_continuation_for(self.fragments_[-1]):
            raise ValueError("Not a continuation")

        self.fragments_.append(update)

    def truncate_until_duration_under(self, limit: timedelta) -> int:
        """
        FIXME almost copy-pasted from hls.MediaPlaylist

        Return value: number of dropped fragments
        """
        if limit.total_seconds() < 0:
            raise ValueError("Negative limit")

        # Find earliest fragment to keep
        leftover_duration = timedelta()
        drop_idx = len(self.fragments_) - 1
        while drop_idx >= 0:
            frag = self.fragments_[drop_idx]
            if leftover_duration + frag.duration > limit:
                break
            else:
                leftover_duration += frag.duration
                drop_idx -= 1

        if drop_idx < 0:
            # May keep everything, nothing to do
            return 0

        # NOTE self.fragments_ may become empty, which most methods don't expect
        self.fragments_ = self.fragments_[drop_idx + 1:]
        return drop_idx + 1

    def drop_prefix_until(self, start: datetime) -> None:
        for idx, frag in enumerate(self.fragments_):
            if frag.start_wall_ts >= start:
                self.fragments_ = self.fragments_[idx:]
                return

        self.fragments_ = []

    def drop_suffix_since(self, end: datetime) -> None:
        for rev_idx, frag in enumerate(reversed(self.fragments_)):
            if frag.end_wall_ts < end:
                left_fragments = len(self.fragments_) - rev_idx
                self.fragments_ = self.fragments_[:left_fragments]
                return

        self.fragments_ = []

    @property
    def start_wall_ts(self) -> datetime:
        return self.fragments_[0].start_wall_ts

    @property
    def end_wall_ts(self) -> datetime:
        return self.start_wall_ts + self.duration

    @property
    def duration(self) -> timedelta:
        return sum(map(lambda f: f.duration, self.fragments_), start=timedelta())

    @property
    def max_fragment_duration(self) -> timedelta:
        return max(map(lambda f: f.duration, self.fragments_))

    @property
    def start_pts(self) -> int:
        return self.fragments_[0].start_pts

    @property
    def end_pts(self) -> int:
        return self.fragments_[-1].end_pts

    @property
    def fragments(self) -> tp.List[MpegTSFile]:
        return self.fragments_
