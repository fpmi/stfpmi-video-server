import logging
import os
import os.path
import typing as tp
from dataclasses import dataclass
from datetime import date, datetime, timedelta
from functools import partial

from prometheus_client import Gauge, Histogram

from stfpmi_video_server.config import Config
from stfpmi_video_server.video.cache import (
    CacheEntryDir,
    FsCache,
    FsCacheSession,
    NopFsCache,
)
from stfpmi_video_server.video.hls import RangesList, MediaPlaylist
from stfpmi_video_server.video.index.abc import Index
from stfpmi_video_server.video.mpegts import MpegTSFile, MpegTSRange


@dataclass
class DirectoryScanState:
    ranges: RangesList
    current_range: tp.List[MpegTSFile]


def from_directory(
    abs_path: str,
    cache_session: FsCacheSession,
    prune_after_days: tp.Optional[int] = None,
    start_date: date = date(1970, 1, 1),
    end_date: date = date(3000, 1, 1),
) -> RangesList:
    if not os.path.isdir(abs_path):
        raise ValueError(f"Not a directory: {abs_path}")

    state = DirectoryScanState(ranges=[], current_range=[])
    now = datetime.now().astimezone()

    def on_mpegts_file(mpegts: MpegTSFile):
        if not state.current_range:
            state.current_range.append(mpegts)
            return

        prev = state.current_range[-1]
        if mpegts.is_continuation_for(prev):
            state.current_range.append(mpegts)
        else:
            state.ranges.append(MpegTSRange(state.current_range))
            state.current_range = [mpegts]

    def n_days_old(n: int, mpegts: MpegTSFile) -> bool:
        return now > (mpegts.end_wall_ts + timedelta(days=n))

    start_date_str = start_date.strftime("%Y-%m-%d")
    end_date_str = end_date.strftime("%Y-%m-%d")

    # No need for recursion: always 2 levels of dirs
    # {per_stream_root}/{date}/{time}.{extension}
    per_date_dirnames = []
    for entry in os.scandir(abs_path):
        if entry.is_dir() and entry.name >= start_date_str and entry.name < end_date_str:
            per_date_dirnames.append(entry.name)
    per_date_dirnames.sort()

    for dirname in per_date_dirnames:
        dirpath = os.path.join(abs_path, dirname)

        cached_dir = cache_session.get_directory(dirpath) or CacheEntryDir(files={}, final=False)
        for mpegts in cached_dir.files.values():
            # Cache guarantees that items order mathes file creation order
            on_mpegts_file(mpegts)

        if cached_dir.final:
            # Directory contents cannot change, no need to loop through files
            continue

        filenames = []
        for entry in os.scandir(dirpath):
            if entry.is_file() and entry.name.endswith(MpegTSFile.EXTENSION):
                filenames.append(entry.name)

        cache_changed = False
        for filename in sorted(filenames):
            if filename in cached_dir.files:
                # Already processed
                continue

            if filename.endswith(".m3u8"):
                # Ignore non-video files
                continue

            try:
                mpegts = MpegTSFile.from_path(os.path.join(dirpath, filename))
                cached_dir.files[filename] = mpegts
                cache_changed = True
            except Exception as e:
                logging.warning(f"Exception analysing {filename} in {dirpath}: {e}")
                continue

            on_mpegts_file(mpegts)

        should_be_final = all(
            map(
                partial(n_days_old, 2),
                cached_dir.files.values(),
            )
        )
        should_be_pruned = prune_after_days and all(
            map(
                partial(n_days_old, prune_after_days),
                cached_dir.files.values(),
            )
        )

        if should_be_pruned:
            logging.info(f"Cache entry to prune: {dirpath}")
            cache_session.prune_directory(dirpath)
        elif should_be_final:
            logging.info(f"Cache entry to mark final: {dirpath}")
            cached_dir.final = True
            cache_session.set_directory(dirpath, cached_dir)
        elif cache_changed:
            cache_session.set_directory(dirpath, cached_dir)

    if state.current_range:
        state.ranges.append(MpegTSRange(state.current_range))

    return state.ranges


class OnDemandIndex(Index):
    def __init__(
        self,
        config: Config,
        stream_id: str,
        *,
        cache: tp.Optional[FsCache] = None,
        last_lookup: Gauge,
        lookup_duration: Histogram,
        last_update: Gauge,
        logger: logging.Logger,
    ) -> None:
        self.stream_id = stream_id
        self.index_dir = config.index_directory_fmt.format(stream_id=stream_id)
        self.segment_uri_prefix = f"{config.segment_uri_prefix}/{stream_id}/"
        self.gap_uri = config.gap_uri
        self.cache_prune_after_days = config.cache_prune_after_days
        self.last_lookup = last_lookup
        self.lookup_duration = lookup_duration
        self.last_update = last_update
        self._logger = logger.getChild('OnDemandIndex')
        self.cache = cache or NopFsCache()

    def close(self) -> None:
        self.cache.close()

    def everything(self) -> str:
        with self.lookup_duration.labels(self.stream_id).time():
            with self.cache.session() as cache_session:
                ranges = from_directory(self.index_dir, cache_session, self.cache_prune_after_days)

        self.last_lookup.labels(self.stream_id).set_to_current_time()
        self.last_update.labels(self.stream_id).set_to_current_time()
        return self._playlist(ranges).render()

    def from_to(self, start: datetime, end: datetime) -> str:
        self._logger.info(f"from_to start={start} end={end}")

        with self.lookup_duration.labels(self.stream_id).time():
            with self.cache.session() as cache_session:
                all_ranges = from_directory(
                    abs_path=self.index_dir,
                    cache_session=cache_session,
                    prune_after_days=self.cache_prune_after_days,
                    start_date=start.date(),
                    end_date=(end + timedelta(days=1)).date(),
                )

        selected_ranges: RangesList = []

        def lower_bound(min_ind, max_ind, checker, x) -> int:
            """
            min_ind and max ind are inclusive min and max index could be taken by checker,
            checker returns 0000111111, returns first index to return 1 by checker
            """
            L = min_ind - 1  # invariant:checker on all indexes <= L returns 0
            R = max_ind + 1  # only returns max_ind + 1 if no index returns 1
            while R - L > 1:
                mid = (L + R) // 2
                if checker(mid, x):
                    R = mid
                else:
                    L = mid
            return R

        for r in all_ranges:
            if r.start_wall_ts > end or r.end_wall_ts < start:
                continue

            def check(ind, x):
                return r.fragments_[ind].start_wall_ts > x

            def soft_check(ind, x):
                return r.fragments_[ind].end_wall_ts >= x

            start_ind = lower_bound(0, len(r.fragments_) - 1, soft_check, start)
            if start_ind == len(r.fragments_):
                continue
            end_ind = lower_bound(0, len(r.fragments_) - 1, check, end)

            r.fragments_ = r.fragments_[start_ind:end_ind]
            if r.fragments:
                selected_ranges.append(r)

        self.last_lookup.labels(self.stream_id).set_to_current_time()
        self.last_update.labels(self.stream_id).set_to_current_time()
        return self._playlist(selected_ranges).render()

    def _playlist(self, ranges: RangesList) -> MediaPlaylist:
        return MediaPlaylist(
            ranges,
            prefix=self.index_dir,
            segment_uri_prefix=self.segment_uri_prefix,
            gap_uri=self.gap_uri,
            live=True,
        )
