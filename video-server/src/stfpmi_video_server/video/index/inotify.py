import logging
import os
import os.path
from dataclasses import dataclass
from datetime import date, datetime, timedelta
from typing import Callable, List, Optional

from prometheus_client import Gauge, Histogram
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

from stfpmi_video_server.config import Config
from stfpmi_video_server.video.cache import (
    CacheEntryDir,
    DictFsCache,
    FsCache,
    FsCacheSession,
)
from stfpmi_video_server.video.hls import RangesList, MediaPlaylist
from stfpmi_video_server.video.index.abc import Index
from stfpmi_video_server.video.mpegts import MpegTSFile, MpegTSRange


@dataclass
class DirectoryScanState:
    ranges: RangesList
    current_range: List[MpegTSFile]


class InotifyHandler(FileSystemEventHandler):
    def __init__(self, cache: FsCache, logger: logging.Logger, on_update: Callable[[], None]) -> None:
        super().__init__()
        self._cache = cache
        self._logger = logger
        self._on_update = on_update

    def _add_file(self, path):
        if isinstance(path, bytes):
            path = path.decode()
        dirpath, filename = os.path.split(path)
        if not filename.endswith(MpegTSFile.EXTENSION):
            return
        with self._cache.session() as cache_session:
            try:
                mpegts = MpegTSFile.from_path(path)
                cached_dir = cache_session.get_directory(dirpath) or CacheEntryDir(
                    files={}, final=False
                )
                cached_dir.files[filename] = mpegts
                # TODO maybe better:
                cached_dir.files = {k: cached_dir.files[k] for k in sorted(cached_dir.files.keys())}
                cache_session.set_directory(dirpath, cached_dir)
                self._logger.info(f"Indexed {filename} in {dirpath}")
                self._on_update()
            except Exception as e:
                self._logger.warning(f"Error indexing {filename} in {dirpath}: {e}")

    def _add_all_files(self, dirpath: str, filenames: List[str]):
        with self._cache.session() as cache_session:
            cache_entry = cache_session.get_directory(dirpath) or CacheEntryDir(files={}, final=False)
            for filename in filenames:
                if filename not in cache_entry.files:
                    try:
                        mpegts = MpegTSFile.from_path(os.path.join(dirpath, filename))
                        cache_entry.files[filename] = mpegts
                        self._logger.info(f"Indexed {filename} in {dirpath}")
                        self._on_update()
                    except Exception as e:
                        self._logger.warning(f"Error indexing {filename} in {dirpath}: {e}")
            # TODO maybe better:
            cache_entry.files = {k: cache_entry.files[k] for k in sorted(cache_entry.files.keys())}
            cache_session.set_directory(dirpath, cache_entry)

    def _del_dir(self, path):
        with self._cache.session() as cache_session:
            cache_session.prune_directory(path)
            self._logger.info(f"Pruned index for {path}")

    def on_created(self, event):
        if not event.is_directory:
            self._add_file(event.src_path)

    def on_deleted(self, event):
        if event.is_directory:  # need to only prune the directories?
            self._del_dir(event.src_path)

    def on_moved(self, event):
        if not event.is_directory:
            # Currently cctv_runner saves unfinished segment as .ts.tmp,
            # so it's not indexed, so no cache change for {event.src_path}
            self._add_file(event.dest_path)

    def on_modified(self, event):
        if not event.is_directory:
            # Currently cctv_runner saves unfinished segment as .ts.tmp,
            # so it's not indexed, so no cache change for {event.src_path}
            self._add_file(
                event.src_path
            )  # I strongly believe files cannot be modified then changing their paths

    def sync_all(self, root_path: str):
        # No need for recursion: always 2 levels of dirs
        # {per_stream_root}/{date}/{time}.{extension}
        per_date_dirnames = []
        for entry in os.scandir(root_path):
            if entry.is_dir():
                per_date_dirnames.append(entry.name)
        per_date_dirnames.sort()

        for dirname in per_date_dirnames:
            dirpath = os.path.join(root_path, dirname)
            filenames = []
            for entry in os.scandir(dirpath):
                if entry.is_file() and entry.name.endswith(MpegTSFile.EXTENSION):
                    filenames.append(entry.name)
            self._add_all_files(dirpath, filenames)


class InotifyIndex(Index):
    def __init__(
        self,
        config: Config,
        stream_id: str,
        *,
        cache: Optional[FsCache] = None,
        last_lookup: Gauge,
        lookup_duration: Histogram,
        last_update: Gauge,
        logger: logging.Logger,
    ) -> None:
        self.stream_id = stream_id
        self.index_dir = config.index_directory_fmt.format(stream_id=stream_id)
        self.segment_uri_prefix = f"{config.segment_uri_prefix}/{stream_id}/"
        self.gap_uri = config.gap_uri
        self.last_lookup = last_lookup
        self.lookup_duration = lookup_duration
        self.last_update = last_update
        self._logger = logger.getChild('InotifyIndex')
        self.cache = cache or DictFsCache()

        self.event_handler = InotifyHandler(
            self.cache,
            self._logger.getChild("InotifyHandler"),
            lambda: self.last_update.labels(self.stream_id).set_to_current_time(),
        )
        self.observer = Observer()
        self.observer.schedule(self.event_handler, self.index_dir, recursive=True)
        self.observer.start()
        self.event_handler.sync_all(self.index_dir)
        self.last_update.labels(self.stream_id).set_to_current_time()

    def close(self) -> None:
        self.observer.stop()
        self.observer.unschedule_all()
        self.cache.close()

    def everything(self) -> str:
        with self.lookup_duration.labels(self.stream_id).time():
            with self.cache.session() as cache_session:
                ranges = self._ranges(
                    cache_session, start_date=date(1970, 1, 1), end_date=date(3000, 1, 1)
                )

        self.last_lookup.labels(self.stream_id).set_to_current_time()
        return self._playlist(ranges).render()

    def from_to(self, start: datetime, end: datetime) -> str:
        self._logger.info(f"from_to start={start} end={end}")
        with self.lookup_duration.labels(self.stream_id).time():
            with self.cache.session() as cache_session:
                all_ranges = self._ranges(
                    cache_session=cache_session,
                    start_date=start.date(),
                    end_date=(end + timedelta(days=1)).date(),
                )

        selected_ranges: RangesList = []

        def lower_bound(min_ind, max_ind, checker, x) -> int:
            """
            min_ind and max ind are inclusive min and max index could be taken by checker,
            checker returns 0000111111, returns first index to return 1 by checker
            """
            L = min_ind - 1  # invariant:checker on all indexes <= L returns 0
            R = max_ind + 1  # only returns max_ind + 1 if no index returns 1
            while R - L > 1:
                mid = (L + R) // 2
                if checker(mid, x):
                    R = mid
                else:
                    L = mid
            return R

        for r in all_ranges:
            if r.start_wall_ts > end or r.end_wall_ts < start:
                continue

            def check(ind, x):
                return r.fragments_[ind].start_wall_ts > x

            def soft_check(ind, x):
                return r.fragments_[ind].end_wall_ts >= x

            start_ind = lower_bound(0, len(r.fragments_) - 1, soft_check, start)
            if start_ind == len(r.fragments_):
                continue
            end_ind = lower_bound(0, len(r.fragments_) - 1, check, end)

            r.fragments_ = r.fragments_[start_ind:end_ind]
            if r.fragments:
                selected_ranges.append(r)

        self.last_lookup.labels(self.stream_id).set_to_current_time()
        return self._playlist(selected_ranges).render()

    def _ranges(
        self,
        cache_session: FsCacheSession,
        start_date: date,
        end_date: date,
    ) -> RangesList:
        state = DirectoryScanState(ranges=[], current_range=[])

        def on_mpegts_file(mpegts: MpegTSFile):
            if not state.current_range:
                state.current_range.append(mpegts)
                return

            prev = state.current_range[-1]
            if mpegts.is_continuation_for(prev):
                state.current_range.append(mpegts)
            else:
                state.ranges.append(MpegTSRange(state.current_range))
                state.current_range = [mpegts]

        start_date_str = start_date.strftime("%Y-%m-%d")
        end_date_str = end_date.strftime("%Y-%m-%d")

        # No need for recursion: always 2 levels of dirs
        # {per_stream_root}/{date}/{time}.{extension}
        per_date_dirnames = []
        for entry in os.scandir(self.index_dir):
            if entry.is_dir() and entry.name >= start_date_str and entry.name < end_date_str:
                per_date_dirnames.append(entry.name)
        per_date_dirnames.sort()

        for dirname in per_date_dirnames:
            dirpath = os.path.join(self.index_dir, dirname)

            cached_dir = cache_session.get_directory(dirpath)
            if cached_dir is None:
                continue
            for mpegts in cached_dir.files.values():
                # Cache guarantees that items order mathes file creation order
                on_mpegts_file(mpegts)

        if state.current_range:
            state.ranges.append(MpegTSRange(state.current_range))

        return state.ranges

    def _playlist(self, ranges: RangesList) -> MediaPlaylist:
        return MediaPlaylist(
            ranges,
            prefix=self.index_dir,
            segment_uri_prefix=self.segment_uri_prefix,
            gap_uri=self.gap_uri,
            live=True,
        )
