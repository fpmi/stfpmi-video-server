# flake8: noqa F401
from stfpmi_video_server.video.index.abc import Index
from stfpmi_video_server.video.index.inotify import InotifyIndex
from stfpmi_video_server.video.index.on_demand import from_directory, OnDemandIndex
