from abc import ABC, abstractmethod
from datetime import datetime


class Index(ABC):
    @abstractmethod
    def everything(self) -> str:
        """
        Render HLS playlist for the whole index
        """
        raise NotImplementedError()

    @abstractmethod
    def from_to(self, start: datetime, end: datetime) -> str:
        raise NotImplementedError()

    def close(self) -> None:
        pass
