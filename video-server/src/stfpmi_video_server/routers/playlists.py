from datetime import date, datetime, time, timedelta
from functools import lru_cache, partial
import logging

from fastapi import APIRouter, HTTPException, Depends, Response
from prometheus_client import Gauge, Histogram
import plyvel

from stfpmi_video_server.config import CacheType, Config, IndexType
from stfpmi_video_server.token import (
    AuthJWTPayload,
    check_auth_token
)
from stfpmi_video_server.video.cache import (
    DictFsCache,
    FsCache,
    LeveldbFsCache,
)
from stfpmi_video_server.video.index import Index, InotifyIndex, OnDemandIndex


class IndexKeeper:
    def __init__(self, config: Config) -> None:
        self._config = config
        self._logger = logging.getLogger("IndexKeeper")

        self._leveldb = None
        if self._config.cache_type == CacheType.PERSISTENT:
            assert self._config.cache_dir
            self._leveldb = plyvel.DB(self._config.cache_dir, create_if_missing=True)

        self.last_lookup = Gauge(
            config.metrics_prefix + 'index_last_lookup',
            'Timestamp of last index lookup',
            ['stream'],
        )
        self.lookup_duration = Histogram(
            config.metrics_prefix + 'index_lookup_duration',
            'Duration of index lookup',
            ['stream'],
        )
        self.last_update = Gauge(
            config.metrics_prefix + 'index_last_update',
            'Timestamp of last index update',
            ['stream'],
        )
        for stream in config.metrics_streams:
            self.last_lookup.labels(stream).set(0)
            self.lookup_duration.labels(stream)
            # Side effects like setting up inotify handlers
            _ = self.get(stream)

    def _get_cache(self, stream_id: str) -> FsCache:
        if self._leveldb:
            prefix = f'fs.{stream_id}.'.encode('utf-8')
            return LeveldbFsCache(self._leveldb.prefixed_db(prefix))
        else:
            return DictFsCache()

    @lru_cache()
    def get(self, stream_id: str) -> Index:
        self._logger.info(f'Creating index for {stream_id}, type {self._config.index_type}')
        if self._config.index_type == IndexType.ONDEMAND:
            return OnDemandIndex(
                self._config,
                stream_id,
                cache=self._get_cache(stream_id),
                last_lookup=self.last_lookup,
                lookup_duration=self.lookup_duration,
                last_update=self.last_update,
                logger=self._logger.getChild(stream_id),
            )
        elif self._config.index_type == IndexType.INOTIFY:
            return InotifyIndex(
                self._config,
                stream_id,
                cache=self._get_cache(stream_id),
                last_lookup=self.last_lookup,
                lookup_duration=self.lookup_duration,
                last_update=self.last_update,
                logger=self._logger.getChild(stream_id),
            )
        else:
            raise ValueError(f"Unsupported index type {self._config.index_type}")


async def get_playlist(
    stream_id: str,
    start_ts: datetime,
    end_ts: datetime,
):
    headers = {"Content-Type": "application/vnd.apple.mpegurl"}
    content = '\n'.join([
        '#EXTM3U',
        '#EXT-X-VERSION:3',
        '#EXT-X-STREAM-INF:BANDWIDTH=126551,CODECS="avc1.4d0014",RESOLUTION=352x240',
        f'{stream_id}/content.m3u8?start_ts={int(start_ts.timestamp())}&end_ts={int(end_ts.timestamp())}',
    ])
    return Response(headers=headers, content=content)


async def get_playlist_content(
    index_keeper: IndexKeeper,
    stream_id: str,
    start_ts: datetime,
    end_ts: datetime,
    token: AuthJWTPayload = Depends(check_auth_token),
):
    """
    Checks that the token has a corresponding ViewPermission

    200: OK + body with content type `application/vnd.apple.mpegurl`

    400: missing/invalid parameters: stream_id, start_ts, end_ts

    403: authorization failed
    """
    if not token.permits(stream_id):
        raise HTTPException(
            status_code=403,
            detail=f"No permission for stream '{stream_id}'",
        )

    index = index_keeper.get(stream_id)
    playlist_str = index.from_to(start_ts, end_ts)

    headers = {"Content-Type": "application/vnd.apple.mpegurl"}
    return Response(headers=headers, content=playlist_str)


async def get_playlist_by_date(
    base_url: str,
    stream_id: str,
    for_date: date,
):
    """
    302: redirect to main handle with correct ts range

    400: missing/invalid parameters: stream_id, for_date
    """
    start_ts = (
        datetime.combine(for_date, time(0, 0, 0))
        .astimezone()  # As server local time
        .timestamp()
    )
    end_ts = start_ts + timedelta(days=1, minutes=2).total_seconds()
    location = f'{base_url}/playlists/{stream_id}?start_ts={int(start_ts)}&end_ts={int(end_ts)}'
    return Response(status_code=302, headers={'location': location})


def make_router(config: Config) -> APIRouter:
    index_keeper = IndexKeeper(config)

    router = APIRouter()
    router.get("/{stream_id}")(get_playlist)
    router.get("/{stream_id}/content.m3u8")(partial(get_playlist_content, index_keeper))
    router.get("/{stream_id}/by_date/{for_date}")(partial(get_playlist_by_date, config.base_url))

    return router
