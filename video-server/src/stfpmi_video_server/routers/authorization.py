import re
import typing as tp
from functools import partial

from fastapi import (
    APIRouter,
    Cookie,
    Header,
    HTTPException,
)

from stfpmi_video_server.config import Config
from stfpmi_video_server.token import check_auth_token


async def auth_segments(
    config: Config,
    x_original_uri: str = Header(default=None),
    auth_token: str = Cookie(default=None),
) -> tp.Dict[str, tp.Any]:
    """
    Checks that the token has a ViewPermission corresponding to the stream_id
    and segment's start/end timestamps

    202: Client authorized to get the segment

    401: Missing authorization token

    403: Client not authorized / missing non-authz parameters / validation error / ...
    """
    if x_original_uri is None:
        raise HTTPException(
            status_code=403,
            detail="Missing header X-Original-URI",
        )
    elif auth_token is None:
        raise HTTPException(
            status_code=401,
            detail="Missing authorization token",
        )

    token = check_auth_token(auth_token)
    regex = ''.join([
        '^',
        config.segment_uri_prefix,
        r"/(?P<stream_id>[a-zA-Z0-9_-]+)",
        r"/(?P<timestamp>[\d-]+/T[\d-]+)",
        r"\.ts$",
    ])
    match = re.fullmatch(regex, x_original_uri)
    if not match:
        raise HTTPException(
            status_code=403,
            detail="URL doesn't match regex",
        )

    stream_id = match['stream_id']

    if token.permits(stream_id):
        return {}
    else:
        raise HTTPException(
            status_code=403,
            detail=f"No permission for stream '{stream_id}'",
        )


def make_router(config: Config):
    router = APIRouter()
    router.get("/", status_code=202)(partial(auth_segments, config))
    return router
