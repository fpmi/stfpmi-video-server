from datetime import datetime, timedelta, timezone
from functools import partial
from secrets import token_urlsafe
from urllib.parse import urlencode

from fastapi import (
    APIRouter,
    HTTPException,
    Request,
)
from fastapi.responses import RedirectResponse, PlainTextResponse
from starlette.middleware.sessions import SessionMiddleware
import jwt
import requests

from stfpmi_video_server.config import Config
from stfpmi_video_server.oidc import IdTokenPayload, OidcIssuer, TokenResponse
from stfpmi_video_server.token import AuthJWTPayload, ViewPermission


AUTH_TOKEN_COOKIE = 'auth_token'
REFRESH_TOKEN_COOKIE = 'svs_refresh_token'
SESSION_COOKIE = 'svs_session'


def redirect_uri(config: Config) -> str:
    return f'{config.base_url}/login/'


def authenticate(config: Config, request: Request, tokens: TokenResponse):
    issuer = OidcIssuer.from_discovery(config.oidc.discovery_url)
    error = request.query_params.get('error')
    if error is not None:
        raise HTTPException(status_code=403, detail=error)

    signing_key = issuer.jwk_client.get_signing_key_from_jwt(tokens.id_token)
    id_token = jwt.api_jwt.decode_complete(
        tokens.id_token,
        algorithms=issuer.metadata.id_token_signing_alg_values_supported,
        key=signing_key.key,
        issuer=issuer.metadata.issuer,
        audience=config.oidc.client_id,
    )
    id_token_payload = IdTokenPayload.parse_obj(id_token['payload'])

    now = datetime.now().astimezone(timezone.utc)
    until = now + timedelta(hours=2)
    auth_token_payload = AuthJWTPayload(
        nbf=now,
        exp=until,
        sub=id_token_payload.sub,
        iss=config.base_url,
        permissions=[
            ViewPermission(
                stream_id=stream,
                start_ts=now,
                end_ts=until,
            ) for stream in id_token_payload.svs_streams
        ],
    )
    auth_token = auth_token_payload.encode(
        config.jwt_algorithm,
        config.jwt_secret_key.get_secret_value(),
    )

    secure = config.base_url.startswith('https://')
    response = RedirectResponse(config.ui_url)
    response.set_cookie(
        AUTH_TOKEN_COOKIE,
        auth_token,
        expires=until,
        secure=secure,
    )
    response.set_cookie(
        REFRESH_TOKEN_COOKIE,
        tokens.refresh_token,
        httponly=True,
        secure=secure,
    )
    return response


def login(config: Config, request: Request):
    issuer = OidcIssuer.from_discovery(config.oidc.discovery_url)

    phase = request.session.get('phase')
    if phase is None:
        state = token_urlsafe(16)
        request.session['state'] = state
        request.session['phase'] = 'code'
        uri = issuer.metadata.authorization_endpoint + '?' + urlencode({
            'response_type': 'code',
            'client_id': config.oidc.client_id,
            'redirect_uri': f'{config.base_url}/login/',
            'state': state,
            'prompt': 'consent',
            'scope': 'openid',
        })
        response = RedirectResponse(uri)
        response.delete_cookie(AUTH_TOKEN_COOKIE)
        response.delete_cookie(REFRESH_TOKEN_COOKIE)
        return response
    elif phase == 'code':
        try:
            auth_code = request.query_params.get('code')
            if auth_code is None:
                raise HTTPException(403, 'No auth code')

            state = request.query_params.get('state')
            if state != request.session.get('state'):
                raise HTTPException(403, 'Invalid state')

            rsp = requests.post(issuer.metadata.token_endpoint, {
                'grant_type': 'authorization_code',
                'client_id': config.oidc.client_id,
                'client_secret': config.oidc.client_secret.get_secret_value(),
                'code': auth_code,
                'redirect_uri': redirect_uri(config),
            })
            rsp.raise_for_status()
            tokens = TokenResponse.parse_obj(rsp.json())

            request.session['phase'] = 'refresh'
            return authenticate(config, request, tokens)
        except Exception:
            response = PlainTextResponse('Internal Server Error', 500)
            response.delete_cookie(AUTH_TOKEN_COOKIE)
            response.delete_cookie(REFRESH_TOKEN_COOKIE)
            if 'state' in request.session:
                del request.session['state']
            if 'phase' in request.session:
                del request.session['phase']
            return response
    elif phase == 'refresh':
        try:
            refresh_token = request.cookies.get(REFRESH_TOKEN_COOKIE)
            if refresh_token is None:
                raise HTTPException(403, 'Missing refresh token')

            rsp = requests.post(issuer.metadata.token_endpoint, {
                'grant_type': 'refresh_token',
                'client_id': config.oidc.client_id,
                'client_secret': config.oidc.client_secret.get_secret_value(),
                'refresh_token': refresh_token,
                'redirect_uri': redirect_uri(config),
            })
            rsp.raise_for_status()
            tokens = TokenResponse.parse_obj(rsp.json())
            return authenticate(config, request, tokens)
        except Exception:
            response = PlainTextResponse('Internal Server Error', 500)
            response.delete_cookie(AUTH_TOKEN_COOKIE)
            response.delete_cookie(REFRESH_TOKEN_COOKIE)
            if 'state' in request.session:
                del request.session['state']
            if 'phase' in request.session:
                del request.session['phase']
            return response
    else:
        response = PlainTextResponse('Invalid State', 400)
        response.delete_cookie(AUTH_TOKEN_COOKIE)
        response.delete_cookie(REFRESH_TOKEN_COOKIE)
        if 'state' in request.session:
            del request.session['state']
        if 'phase' in request.session:
            del request.session['phase']
        return response


def make_router(config: Config):
    router = APIRouter()
    router.get("/")(partial(login, config))
    return SessionMiddleware(
        router,
        session_cookie=SESSION_COOKIE,
        secret_key=config.session_secret.get_secret_value(),
    )
