from argparse import ArgumentParser
from datetime import datetime, timezone

from stfpmi_video_server.config import ConfigHolder
from stfpmi_video_server.token import AuthJWTPayload, ViewPermission


def main():
    parser = ArgumentParser()
    parser.add_argument('-c', '--config', help='.yaml config path', required=False)
    parser.add_argument('--sub', help='Token subject (e.g. ivanov.ii)', required=True)
    parser.add_argument('--iss', help='Token issuer (e.g. admin.ov)', required=True)
    parser.add_argument('--since', help='Start date of permitted period (YYYY-mm-dd, local time)', required=True)
    parser.add_argument('--until', help='End date of permitted period (YYYY-mm-dd, local time)', required=True)
    parser.add_argument('streams', help='Stream to grant access to', nargs='+', metavar='stream')
    args = parser.parse_args()

    config = ConfigHolder.get(yaml_path=args.config)

    since = datetime.strptime(args.since, '%Y-%m-%d').astimezone(timezone.utc)
    until = datetime.strptime(args.until, '%Y-%m-%d').astimezone(timezone.utc)
    now = datetime.now().astimezone(timezone.utc)

    payload = AuthJWTPayload(
        nbf=now,
        exp=until,
        sub=args.sub,
        iss=args.iss,
        permissions=[
            ViewPermission(
                stream_id=stream,
                start_ts=since,
                end_ts=until,
            ) for stream in args.streams
        ],
    )
    token = payload.encode(
      algorithm=config.jwt_algorithm,
      secret_key=config.jwt_secret_key.get_secret_value(),
    )
    print('Token:', token)


if __name__ == '__main__':
    main()
