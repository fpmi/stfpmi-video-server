from argparse import ArgumentParser
from datetime import datetime, timedelta, timezone
from socket import gethostname
import logging
import time

import requests

from stfpmi_video_server.config import Config, ConfigHolder
from stfpmi_video_server.token import AuthJWTPayload, ViewPermission


INITIAL_TIMEOUT = timedelta(minutes=30)
NORMAL_TIMEOUT = timedelta(minutes=2)


def warmup(stream: str, api_url: str, config: Config, long_timeout: bool) -> None:
    issuer = subject = f'stfpmi_video_server@{gethostname()}'
    start = datetime.now()
    since = (start - timedelta(minutes=10)).astimezone(timezone.utc)
    until = (start + timedelta(minutes=2)).astimezone(timezone.utc)

    payload = AuthJWTPayload(
        nbf=since,
        exp=until,
        sub=subject,
        iss=issuer,
        permissions=[
            ViewPermission(
                stream_id=stream,
                start_ts=since,
                end_ts=until,
            ),
        ],
    )
    token = payload.encode(
        algorithm=config.jwt_algorithm,
        secret_key=config.jwt_secret_key.get_secret_value(),
    )
    url = f'{api_url}/playlists/{stream}/content.m3u8'

    response = requests.get(
        url,
        params={'start_ts': since.timestamp(), 'end_ts': until.timestamp()},
        cookies={'auth_token': token},
        timeout=(long_timeout and INITIAL_TIMEOUT or NORMAL_TIMEOUT).total_seconds(),
    )
    status = response.status_code
    logging.info(f'Warmup for stream {stream} took {(datetime.now() - start).total_seconds():.4f} seconds')
    if status != requests.status_codes.codes['ok']:
        raise ValueError(f'Bad status code {status}, body: f{response.text}')


def main():
    logging.basicConfig(level='INFO')

    parser = ArgumentParser()
    parser.add_argument('-c', '--config', help='.yaml config path', required=False)
    parser.add_argument('api_url', help='e.g. http://localhost:8088/svs/api', metavar='api-url')
    parser.add_argument('streams', help='e.g. cam_smoke', nargs='*', metavar='stream')
    parser.add_argument('--initial', help='use a much longer timeout for the initial warmup', action='store_true')
    parser.add_argument('--periodic', help='warmup all streams repeatedly with a fixed interval', action='store_true')
    args = parser.parse_args()

    config = ConfigHolder.get(yaml_path=args.config)

    logging.info(f'API url: {args.api_url}')
    logging.info(f'Is initial (long): {args.initial}')
    logging.info(f'Is periodic: {args.periodic}')

    streams = []
    sleep_sec = 0
    if args.periodic:
        assert len(args.streams) == 0, 'cannot specify stream list together with --periodic'
        assert len(config.cache_warmup_streams) > 0, 'cache_warmup_streams list in config must not be empty'
        streams = config.cache_warmup_streams
        sleep_sec = config.cache_warmup_interval_sec / len(streams)
        logging.info(f'Approx. request interval: {sleep_sec:.0f} seconds')
    else:
        assert len(args.streams) > 0, 'stream list must be specified (unless --periodic is enabled)'
        streams = args.streams

    long_timeout = False
    if args.initial:
        long_timeout = True

    while True:
        for stream in streams:
            try:
                logging.info(f'Starting warmup for stream {stream}...')
                warmup(stream, args.api_url, config, long_timeout=long_timeout)
                logging.info(f'Done {stream}')
            except KeyboardInterrupt:
                raise
            except Exception:
                logging.exception(f'Warmup failed for stream {stream}')
            if sleep_sec > 0:
                logging.info(f'Waiting {sleep_sec:.0f} seconds...')
                time.sleep(sleep_sec)

        if args.periodic:
            # Only first run with long timeout
            long_timeout = False
        else:
            logging.info('Done, use --periodic for periodical checks')
            break


if __name__ == '__main__':
    main()
