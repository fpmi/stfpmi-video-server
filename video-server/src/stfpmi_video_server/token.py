import datetime
import json
import logging
import typing as tp

import jwt
from fastapi import Cookie, HTTPException
from pydantic import BaseModel, Field, ValidationError

from stfpmi_video_server.config import ConfigHolder


class DatetimeJsonEncoder(json.JSONEncoder):
    def default(self, o: tp.Any) -> tp.Any:
        if isinstance(o, datetime.datetime):
            return o.timestamp()
        else:
            return super().default(o)


class ViewPermission(BaseModel):
    stream_id: str
    start_ts: datetime.datetime
    end_ts: datetime.datetime


class AuthJWTPayload(BaseModel):
    not_before: datetime.datetime = Field(alias="nbf")
    expiry: datetime.datetime = Field(alias="exp")
    subject: tp.Optional[str] = Field(alias="sub", default=None)
    issuer: tp.Optional[str] = Field(alias="iss", default=None)
    permissions: tp.List[ViewPermission]

    def encode(self, algorithm: str, secret_key: str) -> str:
        return jwt.encode(  # type: ignore
            self.model_dump(by_alias=True),
            secret_key,
            algorithm=algorithm,
            json_encoder=DatetimeJsonEncoder
        )

    def permits(self, stream_id: str) -> bool:
        return any(map(lambda p: p.stream_id == stream_id, self.permissions))


def check_auth_token(auth_token: tp.Optional[str] = Cookie(None)) -> AuthJWTPayload:
    """
    Parses, validates and returns the authorization JWT from `auth_token`
    cookie.
    Raises if a parse or validation error occurs.
    """
    if auth_token is None:
        raise HTTPException(
            status_code=400,
            detail="Missing auth_token cookie",
        )

    try:
        config = ConfigHolder.get()
        token = jwt.api_jwt.decode_complete(  # type: ignore
            auth_token,
            config.jwt_secret_key.get_secret_value(),
            algorithms=[config.jwt_algorithm],
            options={
                "require": ["nbf", "exp"],
                "verify_nbf": "verify_signature",
                "verify_exp": "verify_signature",
            }
        )
        return AuthJWTPayload.model_validate(token["payload"])
    except ValidationError as e:
        logging.info(f"check_auth_token: Invalid payload: {e}")
        raise HTTPException(
            status_code=403,
            detail=f"Invalid payload: {e}"
        )
    except jwt.exceptions.InvalidTokenError as e:
        logging.info(f"check_auth_token: Invalid JWT token: {e}")
        raise HTTPException(
            status_code=403,
            detail=f"Invalid JWT token: {e}"
        )
