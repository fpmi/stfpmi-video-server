import logging

from fastapi import FastAPI
from prometheus_client import make_asgi_app

from stfpmi_video_server.config import ConfigHolder
from stfpmi_video_server.routers import authorization, playlists, login


def app_factory():
    logging.basicConfig(level=logging.INFO)

    config = ConfigHolder.get()
    app = FastAPI()

    auth_router = authorization.make_router(config)
    app.include_router(auth_router, prefix="/auth")

    playlists_router = playlists.make_router(config)
    app.include_router(playlists_router, prefix="/playlists")

    login_router = login.make_router(config)
    app.mount("/login", login_router)

    app.mount("/metrics", make_asgi_app())

    return app
