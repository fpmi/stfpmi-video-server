import os
import typing as tp
from enum import Enum
from functools import lru_cache

import yaml
from pydantic import SecretStr, model_validator
from pydantic_settings import BaseSettings, SettingsConfigDict


class CacheType(Enum):
    MEMORY = 'memory'
    PERSISTENT = 'persistent'


class IndexType(Enum):
    ONDEMAND = 'ondemand'
    INOTIFY = 'inotify'


class OidcConfig(BaseSettings):
    discovery_url: str
    client_id: str
    client_secret: SecretStr


class Config(BaseSettings):
    # JWT
    jwt_algorithm: str = "HS256"
    jwt_secret_key: SecretStr

    # OIDC
    oidc: OidcConfig
    session_secret: SecretStr

    # Segments
    index_directory_fmt: str
    segment_uri_prefix: str

    # URLs
    base_url: str = ""  # must not end with slash; empty string to serve from /
    ui_url: str
    gap_uri: str  # "blackhole" url for gaps in m3u8

    # Metrics
    metrics_prefix: str = 'svs_'
    metrics_streams: tp.List[str]

    # Index
    index_type: IndexType = IndexType.ONDEMAND

    # Cache
    cache_type: CacheType = CacheType.MEMORY
    cache_dir: tp.Optional[str] = None  # required if cache_type is persistent
    cache_prune_after_days: tp.Optional[int] = None
    cache_warmup_interval_sec: int
    cache_warmup_streams: tp.List[str]

    @model_validator(mode='after')
    def validate(self):
        if self.cache_type == CacheType.PERSISTENT and self.cache_dir is None:
            raise ValueError('cache_dir must be specified for cache_type "persistent"')

        return self

    model_config = SettingsConfigDict(env_prefix='SVS_', case_sensitive=False)


class ConfigHolder:
    CONFIG_PATH_KEY = 'SVS_CONFIG_PATH'

    @classmethod
    @lru_cache()
    def get(cls, yaml_path: tp.Optional[str] = None):
        """
        Returns config instance
        """
        yaml_path = yaml_path or os.getenv(cls.CONFIG_PATH_KEY)
        if yaml_path:
            with open(yaml_path, 'r') as config_file:
                config_object = yaml.safe_load(config_file)
                return Config.model_validate(config_object)
        else:
            return Config()  # type: ignore
