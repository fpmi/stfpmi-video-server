ARG PYTHON_VERSION=3.8
FROM python:${PYTHON_VERSION}-slim-bookworm AS base

FROM base AS deps
RUN apt-get update \
    && apt-get install --no-install-suggests --no-install-recommends -y ffmpeg
RUN ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
ARG HOST_UID
RUN mkdir /work && useradd -m -u ${HOST_UID} user && chown ${HOST_UID} /work
USER ${HOST_UID}
WORKDIR /work
COPY requirements.txt requirements-dev.txt /work/
RUN python -m venv venv \
    && venv/bin/pip download -d wheels build \
    && venv/bin/pip download -d wheels -r requirements.txt \
    && venv/bin/pip download -d wheels -r requirements-dev.txt

FROM deps
COPY --chown=${HOST_UID} src/ /work/src/
COPY --chown=${HOST_UID} tests/ /work/tests/
COPY .flake8 pyproject.toml setup.cfg /work/
RUN venv/bin/pip install --no-index --find-links wheels -e . \
    && venv/bin/pip install --no-index --find-links wheels -r requirements-dev.txt
ENV SVS_CONFIG_PATH=tests/data/config.tests.yaml
ENTRYPOINT []
