### Прежде чем начать

1. Проверить зависимости: [README](README.md#начало-работы)
2. Сгенерировать тестовые сегменты: `make testdata`

### Автотесты

Локальный запуск:

```bash
. venv/bin/activate
. .env
export SVS_CONFIG_PATH
rm -rf tests/data/index
make prepare-for-tests
pytest
pytest -m slow
```

Запуск в контейнере:

```bash
make test
make perftest
```

### Ручной тест

Потребуется бинарник ffplay

1. Запуск приложения:

```bash
. venv/bin/activate
. .env
export SVS_CONFIG_PATH
mkdir -p tests/data/leveldb/general_tests
rm -f tests/data/leveldb/general_tests/*
uvicorn --factory stfpmi_video_server.main:app_factory --host 127.0.0.1 --port 8000
```

2. Запуск веб-сервера для статики:

```bash
cd tests/data/index
python3 -m http.server --bind 127.0.0.1 8080
```

3. Запуск клиента:

```bash
read token <tests/data/test_token.jwt
headers="Cookie: auth_token=$token"$'\r\nContent-Type: application/json\r\n'
url='http://localhost:8000/playlists/stream_0/by_date/1970-01-01'
ffplay -live_start_index -99999 -headers "$headers" -f hls "$url"
```

4. Ожидается, что ffplay покажет 3 сегмента:
   - 30 секунд, счётчик 0..29
   - 25 секунд, счётчик 35..59
   - 30 секунд, счётчик 0..29
