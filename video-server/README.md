## OpenAPI

См. написанную руками [спеку](./docs/openapi.json), TODO(?) генерировать средствами FastAPI

## Эксплуатация

Сгенерировать токен пользователю: `python -m stfpmi_video_server.tools.mktoken -h`

Обновить кеш (следует запускать регулярно): `python -m stfpmi_video_server.tools.warmup -h`

## Разработка

### VSCode

Расширения python и flake8, конфиги в ../.vscode

### Начало работы

Поддерживается Python 3.8-3.12

```
python3 -m venv venv
. venv/bin/activate
python -m pip install -e .
python -m pip install -r requirements-dev.txt
```

### Тесты

См. [TESTING.md](TESTING.md)
